
# Furniro

Backend Documentation for Furniro Furniture Website.

## API Reference

## Authentication - 
### Commands :
### Login

```http
POST api/Authentication/login
```
Type : [FromBody]

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `Email` | `string` | **Required**. |
| `Password` | `string` | **Required**.  |
| `RememberMe` | `bool` | **Optional**.  |

Example Data :
```
{
    "refreshToken": "bY7OEFHmWQ+nWmmVGtkYUTjz4AWQwqwIKCJRb43BKOAZqQaQvwsThRazxbZ61IEq1V6EeHxQ8RUbQQx6j/SH6w==",
    "refreshTokenExpirationDate": "2024-12-30T10:56:17.647498Z",
    "jwt": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiQWxleCBNZXJjZXIiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJTX0FETUlOIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvZW1haWxhZGRyZXNzIjoiYW1lcmNlckBnZW50ZWsuY29tIiwiZXhwIjoxNzM1NTUyNTc3LCJpc3MiOiJodHRwczovL2lkcmlzbWlrYXlpbC5jb20iLCJhdWQiOiJodHRwczovL2lkcmlzbWlrYXlpbC5jb20ifQ.H6GQNMyqy0M0OIkcP-ogwAA-xDf3gt1LQ00DNNSBns8"
}
```
Note : 
Returned data may vary, if RememberMe is false, only JWT will be returned. Token you have received will not be the same as here. (Duh)

### Register

```http
POST api/Authentication/register
```
Type : [FromBody]

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `Fullname` | `string` | **Required**. |
| `Email` | `string` | **Required**. |
| `Password` | `string` | **Required**.  |
| `ConfirmPassword` | `string` | **Required**.  |

Example Return Data :

```
true (or) Exception
```

### Logout

```http
POST api/Authentication/logout
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `Refresh Token` | `string` | **Required**. |

Example Return Data :

```
true (or) Exception
```

### Refresh Token

```http
POST api/Authentication/refresh-token
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `Refresh Token` | `string` | **Required**. |

Example Return Data :

```
string (or) Exception
```

## User -
### Commands :

### Change Password

```http
PUT api/User/changepassword
```

Type: [FromBody] - 
Authorization : **Required**

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `oldPassword` | `string` | **Required**. |
| `newPassword` | `string` | **Required**. |
| `confirmNewPassword` | `string` | **Required**. |
| `email` | `string` | **Required**. |

Example Return Data :

```
true (or) Exception
```


## Blog - 
### Queries :
#### Get all

```http
GET /api/Blog
```
Get all and search use the same method internally. If the keyword is empty, it returns all data. If it isn't it returns data that has the keyword in the Title and (or) Body. If no data with associated keyword is present, returns empty.

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `page` | `int` | **Required**. Defaults to 1. |
| `page_size` | `int` | **Required**.|
| `keyword` | `string` | **Optional**. Keyword. |


#### Get By Id

```http
GET /api/Blog/{id}
```
| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `string(uuid)` | **Required**. Id of item to fetch |

Example Data :
```
{
  "title": "This is an example Blog",
  "body": "This is the body",
  "author": {
    "fullname": "James Clyde",
    "email": "example@example.com"
  },
  "category": {
    "title": "Exterior Design"
  },
  "blogImages": [
    {
      "id": "f3f0b462-a47c-44c1-b07c-8b14fd93133c",
      "imageUrl": "Uploads\Blogabcc4625-a84a-4a58-9a71-fd8585f69410.jfif"
    }
  ]
}
```

#### Get Recent Posts

```http
GET /api/Blog/getrecentposts
```
Returns Blogs sorted in ascending order. Sends out 6 items at once.

#### Get Categories (For Blog)
```http
GET /api/Blog/getcategories
```
Example Data :
```
[
  {
    "title": "Exterior Design",
    "entries": 2
  },
  {
    "title": "Interior Design",
    "entries": 1
  }
]
```
### Commands :

#### Post

```http
POST /api/Blog
```

Type : [FromForm] -
Authorization : **Required**

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `Title` | `string` | **Required**. |
| `Body` | `string` | **Required**.  |
| `AuthorId` | `string(uuid)` | **Required**.  |
| `CategoryId` | `string(uuid)` | **Required**.  |
| `[BlogImageFiles]` | `File` | **Required**. Accepted Formats: .jpg / .jpeg / .webp, .png  |


#### Update
```http
PUT /api/Blog
```
Type : [FromForm] -
Authorization : **Required**

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `Title` | `string` | **Required**. |
| `Body` | `string` | **Required**.  |
| `AuthorId` | `string(uuid)` | **Required**.  |
| `CategoryId` | `string(uuid)` | **Required**.  |

Here is the tricky part. In update, you can update, along with you can also add, and or delete images. These can happen concurrently, meaning you can do them all at once if you want to.

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `ImageId` | `string(uuid)` | **Optional**.  |

Used for Delete. In command this is a list, meaning you can add multiple inside Postman if you want to.

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `ImageDTOs[i].ImageId` | `string(uuid)` | **Required**.  |
| `ImageDTOs[i].ImageFile` | `File` | **Required**. Accepted Formats: .jpg / .jpeg / .webp, .png  |

Used for Update and Add of images. Can also be done concurrently, ImageDTO itself is **NOT REQUIRED**. Here is the catch, if ImageId is given, ImageFile MUST BE ADDED, if ImageId is not given, it will add a new image. Another note. In Postman, you can add ImageId as a column but keep it empty and add ImageFile, this will allow you to add a new file.

#### Delete
```http
DELETE /api/Blog
```
Type : [FromBody] -
Authorization : **Required**


| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `string(uuid)` | **Required**.|


#### Multi Delete
```http
DELETE /api/Blog/multidelete
```
Type : [FromBody] -
Authorization : **Required**

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `[id]`      | `string(uuid)` | **Required**.|

## FAQ - 

#### Why do I get "Internal Server Error 500" and sometimes the Exception ?

Certain data we have that we don't want client side to see is protected behind the generic Internal Server Error 500. This could include Database exceptions, NotImplementedExceptions and other.