﻿using Domain.Entities;
using Domain.Entities.Products;
using Domain.Entities.UserRelated;
using Domain.Rules;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Infrastructure.Data
{
    public  class ApplicationDBContext : DbContext
    {
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options)
        {
           
        }

        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Newsletter> NewsletterSubscriptions { get; set; }

        #region Product
        public DbSet<Product> Products { get; set; }
        public DbSet<DescriptionOfProduct> Descriptions { get; set; }
        public DbSet<ProductColor> ProductColor { get; set; }
        public DbSet<ProductSize> ProductSize { get; set; }
        public DbSet<ProductTag> ProductTag { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Color> Colors { get; set; }
        public DbSet<Size> Sizes { get; set; }
        public DbSet<ShoppingCart> ShoppingCart { get; set; }
        public DbSet<CartItem> CartItems { get; set; }
        public DbSet<WishList> WishList { get; set; }
        public DbSet<Stock> Stock { get; set; }

        #endregion
        #region Images
        public DbSet<Images<Product>> ProductImages { get; set; }
        public DbSet<Images<DescriptionOfProduct>> DescriptionImages { get; set; }
        public DbSet<Images<Blog>> BlogImages { get; set; }
        #endregion
        #region Checkout
        public DbSet<Checkout> Orders { get; set; }
        public DbSet<State> Provinces { get; set; }
        public DbSet<Country> Countries { get; set; }
        #endregion
        #region User / Authentication / Authorization
        public DbSet<User> Users { get; set; }
        public DbSet<Role> UserRoles { get; set; }
        #endregion
        #region Pages
        #region Contact
        public DbSet<ContactInqury> Leads { get; set; }
        public DbSet<ContactDetails> SiteContactDetails { get; set; }
        #endregion
        #endregion
        #region Auto Data Generation
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            foreach (var entityType in modelBuilder.Model.GetEntityTypes())
            {
                if (typeof(ISoftDeletable).IsAssignableFrom(entityType.ClrType))
                {
                    ParameterExpression? parameter = Expression.Parameter(entityType.ClrType, "e");
                    MemberExpression? property = Expression.Property(parameter, "IsDeleted");
                    LambdaExpression? filter = Expression.Lambda(Expression.Not(property), parameter);
                    modelBuilder.Entity(entityType.ClrType).HasQueryFilter(filter);
                }
            }

            SeedData(modelBuilder: modelBuilder);
        }

        private static void SeedData(ModelBuilder modelBuilder)
        {
            GenerateUserRoles(modelBuilder);
            GenerateContactDetails(modelBuilder);
            GenerateCountries(modelBuilder);
            GenerateStates(modelBuilder);
            
        }
        private static void GenerateUserRoles(ModelBuilder modelBuilder)
        {
            if (!modelBuilder.Model.GetEntityTypes().Any(e => e.ClrType.Name == "Roles"))
            {
                modelBuilder.Entity<Role>().HasData(
                    new Role { Id = Guid.NewGuid(), Title = "S_ADMIN", Created = DateTime.UtcNow, Updated = DateTime.UtcNow },
                    new Role { Id = Guid.NewGuid(), Title = "ADMIN", Created = DateTime.UtcNow, Updated = DateTime.UtcNow },
                    new Role { Id = Guid.NewGuid(), Title = "MODERATOR", Created = DateTime.UtcNow, Updated = DateTime.UtcNow },
                    new Role { Id = Guid.Parse("8478634D-E644-46C1-BBBC-A2D40166CB0E"), Title = "USER", Created = DateTime.UtcNow, Updated = DateTime.UtcNow }
                );;
            }
        }
        #region Countries / Provinces
        private static void GenerateCountries(ModelBuilder modelBuilder)
        {
            if (!modelBuilder.Model.GetEntityTypes().Any(e => e.ClrType.Name == "Countries"))
            {
                modelBuilder.Entity<Country>().HasData(
                    new Country { Id = Guid.Parse("6fb4e784-72c1-4b31-9f21-85f28c52a12d"), Name = "The United States of America", Created = DateTime.UtcNow, Updated = DateTime.UtcNow },
                    new Country { Id = Guid.Parse("f37e185d-d30f-42b4-b7bf-6f66b8b85515"), Name = "Germany", Created = DateTime.UtcNow, Updated = DateTime.UtcNow }
                );
            }
        }
        //A.K.A Provinces
        private static void GenerateStates(ModelBuilder modelBuilder)
        {
            if (!modelBuilder.Model.GetEntityTypes().Any(e => e.ClrType.Name == "Provinces"))
            {
                #region Germany
                modelBuilder.Entity<State>().HasData(
                new State { Id = Guid.NewGuid(), Title = "Baden-Württemberg", CountryId = Guid.Parse("f37e185d-d30f-42b4-b7bf-6f66b8b85515"),Created = DateTime.UtcNow, Updated = DateTime.UtcNow },
                new State { Id = Guid.NewGuid(), Title = "Bavaria", CountryId = Guid.Parse("f37e185d-d30f-42b4-b7bf-6f66b8b85515"), Created = DateTime.UtcNow, Updated = DateTime.UtcNow },
                new State { Id = Guid.NewGuid(), Title = "Berlin", CountryId = Guid.Parse("f37e185d-d30f-42b4-b7bf-6f66b8b85515"), Created = DateTime.UtcNow, Updated = DateTime.UtcNow });
                #endregion
                #region The United States of America
                modelBuilder.Entity<State>().HasData(
                new State { Id = Guid.NewGuid(), Title = "Kentucky", CountryId = Guid.Parse("6fb4e784-72c1-4b31-9f21-85f28c52a12d"), Created = DateTime.UtcNow, Updated = DateTime.UtcNow },
                new State { Id = Guid.NewGuid(), Title = "New York", CountryId = Guid.Parse("6fb4e784-72c1-4b31-9f21-85f28c52a12d"), Created = DateTime.UtcNow, Updated = DateTime.UtcNow },
                new State{ Id = Guid.NewGuid(), Title = "Texas", CountryId = Guid.Parse("6fb4e784-72c1-4b31-9f21-85f28c52a12d"),Created = DateTime.UtcNow, Updated = DateTime.UtcNow });
                #endregion
            }
        }
        #endregion
        private static void GenerateContactDetails(ModelBuilder modelBuilder)
        {
            if (!modelBuilder.Model.GetEntityTypes().Any(e => e.ClrType.Name == "SiteContactDetails"))
            {
                modelBuilder.Entity<ContactDetails>().HasData(
                    new ContactDetails{
                        Id = Guid.NewGuid(),
                        PhoneNumberHotline = "+1 888 888 888",
                        PhoneNumberMobile = "+1 888 888 888",
                        WorkHoursOnWeekdays = "09:00 - 18:00",
                        WorkHoursOnWeekends = "09:00 - 14:00",
                        Address = "236 5th SE Avenue, New York NY10000, United States",
                        Created = DateTime.UtcNow,
                        Updated = DateTime.UtcNow
                    });
            }
        }
        #endregion
    }
}
