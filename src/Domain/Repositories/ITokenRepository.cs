﻿using Domain.Entities.UserRelated;

namespace Domain.Repositories
{
    public interface ITokenRepository
    {
        public Task<string> GenerateRefreshToken(User user);
        public Task<string> GenerateJWTToken(User user);
    }
}