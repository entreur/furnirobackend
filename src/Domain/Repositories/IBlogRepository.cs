﻿using Domain.Entities;

namespace Domain.Repositories
{
    public interface IBlogRepository : IRepository<Blog>
    {
    }
}
