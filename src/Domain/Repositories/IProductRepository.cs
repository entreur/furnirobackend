﻿using Domain.Entities.Products;

namespace Domain.Repositories
{
    public interface IProductRepository : IRepository<Product>
    {

    }
}