﻿using Domain.Entities.Products;

namespace Domain.Repositories
{
    public interface IShoppingCartRepository : IRepository<ShoppingCart>
    {
        //Task<ShoppingCart> GetByUserIdAsync(Guid userId);
    }
}