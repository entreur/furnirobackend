﻿using Domain.Entities.UserRelated;

namespace Domain.Repositories
{
    public interface IAuthenticationRepository : IRepository<User>
    {
        
    }
}
