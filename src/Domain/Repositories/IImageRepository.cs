﻿using Domain.Entities;
using Microsoft.AspNetCore.Http;

namespace Domain.Repositories
{
    /// <summary>
    /// Generic ImageUpload Repository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IImageRepository<T> where T : BaseEntity
    {
        Task AddImage(IEnumerable<IFormFile> images, Guid id);
        Task UpdateImage(Images<T> entity);
        Task DeleteImage(IEnumerable<Images<T>> images);
    }
}
