﻿using Microsoft.AspNetCore.Http;

namespace Application.Repositories
{
    public interface IFileManagerRepository
    {
        Task<string> Upload(IFormFile file, string _PATH);
        Task<IEnumerable<string>> MultiUpload(IEnumerable<IFormFile> files, string _PATH);
        void DeleteFile(string fileName);
    }
}
