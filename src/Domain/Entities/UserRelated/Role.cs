﻿using System.ComponentModel.DataAnnotations;


namespace Domain.Entities.UserRelated
{
    public class Role : BaseEntity
    {
        [Required]
        public string Title { get; set; }
    }
}