﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities.UserRelated
{
    public class User : BaseEntity
    {
        [Required]
        public string Fullname { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }

        public string? ProfileImageUrl { get; set; } = string.Empty;
        public string? RefreshToken { get; set; }
        public DateTime? RefreshTokenExpirationDate { get; set; }

        [ForeignKey("RoleId")]
        public Role UserRole { get; set; }
        //By default all users are role of USER.
        public Guid RoleId { get; set; } = Guid.Parse("8478634D-E644-46C1-BBBC-A2D40166CB0E");
    }
}
