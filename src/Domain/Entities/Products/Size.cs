﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Products
{
    public class Size : BaseEntity
    {
        //public Size()
        //{
            
        //}
        //public Size(string title,List<ProductSize> productSizes)
        //{
        //    Title = title;
        //    ProductSize = productSizes;
        //}
        [Required]
        public string Title { get; set; }
        [Required]
        public List<ProductSize> ProductSize { get; set; }
    }
}
