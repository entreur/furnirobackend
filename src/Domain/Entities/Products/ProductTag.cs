﻿namespace Domain.Entities.Products
{
    /// <summary>
    /// Many to Many Association between Product and Tag
    /// </summary>
    public class ProductTag : BaseEntity
    {
        public Guid ProductId { get; set; }
        public Guid TagId { get; set; }
        public Product Product { get; set; }
        public Tag Tag { get; set; }
    }
}
