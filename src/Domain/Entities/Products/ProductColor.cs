﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities.Products
{
    /// <summary>
    /// Many to Many Association between Product and Size
    /// </summary>
    public class ProductColor : BaseEntity
    {
        public ProductColor()
        {
            
        }

        public Guid ProductId { get; set; }
        public Guid ColorId { get; set; }
        [ForeignKey("ProductId")]
        public Product Product { get; set; }
        [ForeignKey("ColorId")]
        public Color Color { get; set; }
        [Required]
        public int Quantity { get; set; }
    }
}
