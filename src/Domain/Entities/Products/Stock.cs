﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Products
{
    public class Stock : BaseEntity
    {
        public Stock()
        {
            
        }

        [Required]
        public Guid ProductId { get; set; }
        [Required]
        [ForeignKey("ProductId")]
        public Product Product { get; set; }
        [Required]
        public Guid SizeId { get; set; }
        [Required]
        [ForeignKey("SizeId")]
        public Size Size { get; set; }
        [Required]
        public Guid ColorId { get; set; }
        [Required]
        [ForeignKey("ColorId")]
        public Color Color { get; set; }
        [Required]
        public int Quantity { get; set; }
    }
}
