﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities.Products
{
    public class DescriptionOfProduct : BaseEntity
    {
        [Required]
        public string Text { get; set; }
        public List<Images<DescriptionOfProduct>> DescriptionImages { get; set; }
        public Product Product { get; set; }
    }
}
