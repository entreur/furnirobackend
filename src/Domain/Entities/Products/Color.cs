﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities.Products
{
    public class Color : BaseEntity
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string HexCode { get; set; }
        [Required]
        public List<ProductColor> ProductColor { get; set; }
    }
}
