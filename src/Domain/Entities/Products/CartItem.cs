﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities.Products
{
    public class CartItem : BaseEntity
    {
        [ForeignKey("ProductId")]
        public Product Product { get; set; }
        public Guid ProductId { get; set; }
        [ForeignKey("SizeId")]
        public Size Size { get; set; }
        public Guid SizeId { get; set; }
        [ForeignKey("ColorId")]
        public Color Color { get; set; }
        public Guid ColorId { get; set; }
        public short Quantity { get; set; }
    }
}