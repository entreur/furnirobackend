﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities.Products
{
    public class Product : BaseEntity
    {
        [Required]
        public string Name { get; set; }
        [Required]
        [MaxLength(8,ErrorMessage = "SKU's cannot exceed 8.")]
        public string SKU { get; set; }
        [Required]
        public double Price { get; set; }
        public double? DiscountPercentage { get; set; }
        [Required]
        public string Summary { get; set; }
        [Required]
        public string AdditionalInformation { get; set; }
        [Required]
        public Guid CategoryId { get; set; }
        [Required]
        [ForeignKey("CategoryId")]
        public Category Category { get; set; }
        [ForeignKey("DescriptionOfProductId")]
        public DescriptionOfProduct DescriptionOfProduct { get; set; }
        public Guid DescriptionOfProductId { get; set; }
        public List<Review> Reviews { get; set; }
        [Required]
        public List<ProductColor> ProductColor { get; set; }
        [Required]
        public List<ProductTag> ProductTag { get; set; }
        [Required]
        public List<ProductSize> ProductSize { get; set; }
        public List<Images<Product>> ProductImages { get; set; }
        [Required]
        public List<Stock> Stocks { get; set; }
    }
}
