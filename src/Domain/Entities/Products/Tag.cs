﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities.Products
{
    public class Tag : BaseEntity
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public List<ProductTag> ProductTag { get; set; }
    }
}
