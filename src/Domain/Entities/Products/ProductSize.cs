﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities.Products
{
    /// <summary>
    /// Many to Many Association between Product and Size
    /// </summary>
    public class ProductSize : BaseEntity
    {
        public ProductSize()
        {
            
        }

        public Guid ProductId { get; set; }
        public Guid SizeId { get; set; }
        public Product Product { get; set; }
        public Size Size { get; set; }
        [Required]
        public int Quantity { get; set; }
    }
}
