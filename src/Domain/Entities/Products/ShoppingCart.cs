﻿namespace Domain.Entities.Products
{
    public class ShoppingCart : BaseEntity
    {
        public Guid UserId { get; set; }
        public List<CartItem> Items { get; set; }
        public List<WishList> WishListItems { get; set; }
    }
}