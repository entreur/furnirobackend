﻿using Domain.Entities.Products;
using Domain.Entities.UserRelated;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Checkout : BaseEntity
    {
        [Required]
        public string Fullname { get; set; }
        public string? CompanyName { get; set; } = string.Empty;
        [Required]
        [ForeignKey("UserId")]
        public User User { get; set; }
        [Required]
        public Guid UserId { get; set; }
        [Required]
        [ForeignKey("CountryId")]
        public Country Country { get; set; }
        [Required]
        public Guid CountryId { get; set; }
        [Required]
        [ForeignKey("StateId")]
        public State State { get; set; }
        [Required]
        public Guid StateId { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string ZipCode { get; set; }
        [Required]
        [Phone]
        public string PhoneNumber { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [MaxLength(1000,ErrorMessage = "Cannot exceed 1000")]
        public string? AdditionalInfo { get; set; } = string.Empty;
        //Shopping Cart
        [Required]
        [ForeignKey("ShoppingCartId")]
        public ShoppingCart Cart { get; set; }
        [Required]
        public Guid ShoppingCartId { get; set; }
        [Required]
        public bool OrderIsConfirmed { get; set; } = false;
    }
}