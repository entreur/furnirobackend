﻿using Domain.Rules;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    /// <summary>
    /// Model of contact inquiries sent by users. A.K.A Leads.
    /// </summary>
    public class ContactInqury : BaseEntity, ISoftDeletable
    {
        [Required]
        public string Fullname { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "Not a valid email address")]
        public string Email { get; set; }
        [Required]
        [MaxLength(250, ErrorMessage = "Cannot exceed 250")]
        public string Subject { get; set; }
        [Required]
        [MaxLength(1000, ErrorMessage = "Cannot exceed 1000")]
        public string Message { get; set; }
        public bool IsDeleted { get; set; }
    }
}