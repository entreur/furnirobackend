﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    /// <summary>
    /// Model of the contact details of the site.
    /// </summary>
    public class ContactDetails : BaseEntity
    {
        [Required]
        [Phone]
        public string PhoneNumberHotline { get; set; }
        [Required]
        [Phone]
        public string PhoneNumberMobile { get; set; }
        [Required]
        public string WorkHoursOnWeekdays { get; set; }
        [Required]
        public string WorkHoursOnWeekends { get; set; }
        [Required]
        public string Address { get; set; }
    }
}