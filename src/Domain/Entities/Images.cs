﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities 
{
    /// <summary>
    /// One To Many relation for product and it's images.
    /// </summary>
    public class Images<T> : BaseEntity where T : BaseEntity
    {
        [Required]
        [ForeignKey("Item")]
        public Guid ItemId { get; set; }
        [Required]
        public T Item { get; set; }
        [Required]
        public string ImageUrl { get; set; }
    }
}
