﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Country : BaseEntity
    {
        [Required]
        public string Name { get; set; }
        public List<State> States { get; set; }
    }
}