﻿using Domain.Entities.Products;
using Domain.Entities.UserRelated;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Blog : BaseEntity
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string Body { get; set; }
        [Required]
        public Guid AuthorId { get; set; }
        [Required]
        [ForeignKey("AuthorId")]
        public User Author { get; set; }
        [Required]
        public Guid CategoryId { get; set; }
        [Required]
        [ForeignKey("CategoryId")]
        public Category Category { get; set; }
        public List<Images<Blog>> BlogImages { get; set; }
    }
}
