﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    /// <summary>
    /// Base of all entities in the application.
    /// </summary>
    public abstract class BaseEntity
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public DateTime Created { get; set; }
        [Required]
        public DateTime Updated { get; set; }
    }
}
