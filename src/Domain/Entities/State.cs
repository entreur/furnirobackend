﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class State : BaseEntity
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public Guid CountryId { get; set; }
        [Required]
        [ForeignKey("CountryId")]
        public Country Country { get; set; }
    }
}
