﻿using Domain.Rules;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class Newsletter : BaseEntity, ISoftDeletable
    {
        [Required]
        public string Email { get; set; }
        public bool IsDeleted { get; set; } = false;
        //IsReceiver means that the user is subscribed to the newsletter and will receive emails.
        public bool IsReceiver { get; set; } = true;   
    }
}
