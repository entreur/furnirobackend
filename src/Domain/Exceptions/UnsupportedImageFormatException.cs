﻿using Domain.Rules;

namespace Domain.Exceptions
{
    public class UnsupportedImageFormatException : Exception, INonSensitiveExceptions
    {
        public UnsupportedImageFormatException()
            : base("File format is not .png, .webp, or .jpg / .jpeg")
        {

        }
    }
}
