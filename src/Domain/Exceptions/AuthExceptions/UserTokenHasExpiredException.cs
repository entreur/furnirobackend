﻿using Domain.Rules;

namespace Domain.Exceptions.AuthExceptions
{
    public class UserTokenHasExpiredException<User> : Exception, INonSensitiveExceptions
    {
        public UserTokenHasExpiredException() : base($"Token has expired, please login again.")
        {
        }
    }
}
