﻿using Domain.Rules;

namespace Domain.Exceptions.AuthExceptions
{
    public class NewOldPasswordCantBeSameException<User> : Exception, INonSensitiveExceptions
    {
        public NewOldPasswordCantBeSameException() : base("New password can't be the same as the old password")
        {
        }
    }
}
