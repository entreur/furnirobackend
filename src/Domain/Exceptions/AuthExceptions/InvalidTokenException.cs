﻿using Domain.Rules;

namespace Domain.Exceptions.AuthExceptions
{
    public class InvalidTokenException : Exception, INonSensitiveExceptions
    {
        public InvalidTokenException(string token) : base($"Invalid token: {token}")
        {
        }
    }
}