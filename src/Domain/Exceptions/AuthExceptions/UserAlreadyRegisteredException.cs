﻿using Domain.Rules;

namespace Domain.Exceptions
{
    public class UserAlreadyRegisteredException: Exception, INonSensitiveExceptions
    {
        public object Email { get; }

        public UserAlreadyRegisteredException(object email)
            : base($"{email} is already registered")
        {
            Email = email;
        }
    }
}
