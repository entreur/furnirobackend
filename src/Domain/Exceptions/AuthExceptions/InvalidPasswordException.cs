﻿using Domain.Rules;

namespace Domain.Exceptions.AuthExceptions
{
    public class InvalidPasswordException<User> : Exception, INonSensitiveExceptions
    {
        public InvalidPasswordException() : base("Password is Incorrect")
        {

        }
    }
}
