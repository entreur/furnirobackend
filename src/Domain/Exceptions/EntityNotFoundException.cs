﻿using Domain.Rules;

namespace Domain.Exceptions
{
    /// <summary>
    /// Client side exception.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EntityNotFoundException<T> : Exception, INonSensitiveExceptions
    {
        public object Id { get; }

        public EntityNotFoundException(object id)
            : base($"{typeof(T).Name} with identifier {id} could not be found.")
        {
            Id = id;
        }

        public EntityNotFoundException()
            : base($"{typeof(T).Name} could not be found.")
        {
        }
    }
}
