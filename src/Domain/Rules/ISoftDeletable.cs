﻿namespace Domain.Rules
{
    public interface ISoftDeletable
    {
        bool IsDeleted { get; set; }
    }
}
