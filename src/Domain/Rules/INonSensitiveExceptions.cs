﻿namespace Domain.Rules
{
    /// <summary>
    /// Exception types that can be exposed to the client
    /// </summary>
    public interface INonSensitiveExceptions
    {

    }
}
