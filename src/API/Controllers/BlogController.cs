﻿using Application.BlogDetails.BlogQueries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlogController : ControllerBase
    {
        private readonly IMediator _mediator;

        public BlogController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllBlogs([FromQuery] string? keyword,short page,short page_size)
        {
            return Ok(await _mediator.Send(new GetAllBlogsQuery(keyword,page,page_size)));
        }

        [HttpGet("/api/[controller]/{id}")]
        public async Task<IActionResult> GetBlogById(Guid id)
        {
            return Ok(await _mediator.Send(new GetBlogByIdQuery(id)));
        }

        [HttpGet]
        [Route("/api/[controller]/getrecentposts")]
        public async Task<IActionResult> GetRecentBlogs()
        {
            return Ok(await _mediator.Send(new GetRecentBlogPostsQuery()));
        }

        [HttpGet]
        [Route("/api/[controller]/getcategories")]
        public async Task<IActionResult> GetCategories()
        {
            return Ok(await _mediator.Send(new GetBlogCategoriesQuery()));
        }
    }
}
