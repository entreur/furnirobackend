﻿using Application.NewsletterDetails.Commands;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NewsletterController : ControllerBase
    {
        private readonly IMediator _mediator;
        public NewsletterController(IMediator mediator)
        {
            _mediator = mediator;
        }
        [HttpPost]
        [Route("/api/[controller]/subscribe")]
        public async Task<IActionResult> CreateSubscription([FromBody] CreateSubscriptionCommand command)
        {
            return Ok(await _mediator.Send(command));
        }
        [HttpPatch]
        [Route("/api/[controller]/unsubscribe")]
        public async Task<IActionResult> Unsubscribe([FromBody] UnsubscribeFromNewsletterCommand command)
        {
            return Ok(await _mediator.Send(command));
        }
    }
}