﻿using Application.BlogDetails.BlogQueries;
using Application.ProductDetails.ProductQueries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ProductController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] string? keyword, short page, double minPrice, double maxPrice, string? size, string? color)
        {
            return Ok(await _mediator.Send(new GetAllProductsQuery(keyword, page, minPrice,maxPrice,size,color)));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetProductById(Guid id)
        {
            return Ok(await _mediator.Send(new GetProductByIdQuery(id)));
        }
    }
}