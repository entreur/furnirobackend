﻿using Application.AboutDetails.Commands;
using Application.AboutDetails.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        private readonly IMediator _mediator;
        public ContactController(IMediator mediator)
        {
            _mediator = mediator;
        }
        [HttpPost]
        public async Task<IActionResult> SendContactInqury([FromBody] ContactUsCommand command)
        {
            return Ok(await _mediator.Send(command));
        }
        [HttpGet]
        [Route("/api/[controller]/getcontactdetails")]
        public async Task<IActionResult> GetContactDetails()
        {
            return Ok(await _mediator.Send(new GetContactDetailsQuery()));
        }
    }
}
