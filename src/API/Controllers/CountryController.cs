﻿using Application.CountryAndStateDetails.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountryController : ControllerBase
    {
        private readonly IMediator _mediator;
        public CountryController(IMediator mediator)
        {
            _mediator = mediator;
        }
        [HttpGet]
        public async Task<IActionResult> GetAllCountriesAndStates()
        {
            return Ok(await _mediator.Send(new GetAllCountriesAndStatesQuery()));
        }
    }
}