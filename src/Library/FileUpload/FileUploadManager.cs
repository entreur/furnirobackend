﻿using Application.Repositories;
using Domain.Exceptions;
using Microsoft.AspNetCore.Http;

namespace Library.FileUpload
{
    public class FileUploadManager : IFileManagerRepository
    {
        private readonly string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        public async Task<IEnumerable<string>> MultiUpload(IEnumerable<IFormFile> files, string path)
        {
            List<string> fileNames = new();
            foreach (IFormFile? file in files)
            {
                fileNames.Add(await Upload(file, path));
            }
            return fileNames.AsEnumerable();
        }

        public async Task<string> Upload(IFormFile file, string path)
        {

            if (!FileValidationManager.IsImageFile(file))
            {
                throw new UnsupportedImageFormatException();
            }

            string writePath = GetWritePath(path);
            string newName = GenerateUniqueFileName(file);
            GenerateFolder(path: writePath);
            using FileStream stream = new(Path.Combine(writePath, newName), FileMode.Create);
            await file.CopyToAsync(stream);
            return $"{desktopPath}\\{path}\\{newName}";
        }
        void IFileManagerRepository.DeleteFile(string fileName)
        {
            string path = $"{GetWritePath(fileName)}";
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            File.Delete(path);
        }
        #region Private Methods
        private static void GenerateFolder(string path)
        {
            Directory.CreateDirectory(path);
        }
        private static string GenerateUniqueFileName(IFormFile file)
        {
            string extension = file.FileName.Split('.').Last();
            return $"{Guid.NewGuid()}.{extension}";
        }
        private string GetWritePath(string path)
        {
            return Path.Combine(desktopPath, path);
        }
        #endregion
    }
}