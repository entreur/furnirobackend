﻿using Domain.Entities.UserRelated;
using Domain.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace Library.Authentication
{
    public class TokenManager : ITokenRepository
    {
        public TokenManager(IConfiguration conf)
        {
            _conf = conf;
        }
        private readonly IConfiguration _conf;

        public async Task<string> GenerateJWTToken(User user)
        {
            List<Claim> claims = new()
            {
                new Claim(ClaimTypes.Name,user.Fullname),
                new Claim(ClaimTypes.Role, user.UserRole.Title),
                new Claim(ClaimTypes.Email, user.Email)
            };

            SymmetricSecurityKey? signingKey = new(Encoding.UTF8.GetBytes(_conf["JWT:Key"]));
            SigningCredentials? signingCredentials = new(signingKey, SecurityAlgorithms.HmacSha256);
            JwtSecurityToken? token = new(
                issuer: _conf["JWT:Issuer"],
                audience: _conf["JWT:Audience"],
                claims: claims,
                expires: DateTime.Now.AddYears(1),
                signingCredentials: signingCredentials
                );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public async Task<string> GenerateRefreshToken(User user)
        {
            user.RefreshToken = Convert.ToBase64String(RandomNumberGenerator.GetBytes(64));
            user.RefreshTokenExpirationDate = DateTime.UtcNow.AddYears(1).AddHours(1);
            return user.RefreshToken;
        }
    }
}
