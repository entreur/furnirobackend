﻿using Application.UserDetails.Commands;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AdminPanel.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "USER,MODERATOR,S_ADMIN,ADMIN")]
    public class UserController : ControllerBase
    {
        private readonly IMediator _mediator;

        public UserController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPut]
        [Route("/api/[controller]/changepassword")]
        [Authorize]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        [HttpPost]
        [Route("/api/[controller]/uploadprofilepicture")]
        [Authorize]
        public async Task<IActionResult> UploadProfilePicture([FromForm] ProfilePictureCommand command)
        {
            return Ok(await _mediator.Send(command));
        }
    }
}