﻿using Application.NewsletterDetails.Commands;
using Application.NewsletterDetails.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AdminPanel.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "S_ADMIN,ADMIN")]
    public class NewsletterController : ControllerBase
    {
        private readonly IMediator _mediator;
        public NewsletterController(IMediator mediator)
        {
            _mediator = mediator;
        }
        [HttpGet]
        [Route("/api/[controller]/getallsubscribers")]
        public async Task<IActionResult> GetAllNewsletter([FromQuery] GetAllNewsletterQuery request)
        {
            return Ok(await _mediator.Send(request));
        }
        [HttpPatch]
        [Route("/api/[controller]/deletesubscriber")]
        //Same as in contact inqury, this also soft deletes, so it is a put request.
        public async Task<IActionResult> DeleteNewsletterSubscriber([FromBody] DeleteNewsletterCommand request)
        {
            return Ok(await _mediator.Send(request));
        }
    }
}
