﻿using Application.CheckoutDetails.Commands;
using Application.CheckoutDetails.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AdminPanel.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "ADMIN,S_ADMIN")]
    public class OrderController : ControllerBase
    {
        private readonly IMediator _mediator;
        public OrderController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> CheckAllOrders([FromQuery] GetAllOrdersQuery request)
        {
            return Ok(await _mediator.Send(request));
        }

        [HttpGet]
        [Route("/api/[controller]/getorderbyid")]
        public async Task<IActionResult> GetOrderById([FromQuery] GetOrderByIdRequest request)
        {
            return Ok(await _mediator.Send(request));
        }

        [HttpPatch]
        [Route("/api/[controller]/confirmorder")]
        public async Task<IActionResult> ConfirmOrder([FromBody] ConfirmOrderCommand request)
        {
            return Ok(await _mediator.Send(request));
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteOrder([FromBody] DeleteOrderCommand request)
        {
            return Ok(await _mediator.Send(request));
        }
    }
}