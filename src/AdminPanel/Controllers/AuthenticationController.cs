﻿using Application.AuthenticationDetails.Commands;
using Application.AuthenticationDetails.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace AdminPanel.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IMediator _mediator;

        public AuthenticationController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        [Route("/api/[controller]/login")]
        public async Task<IActionResult> Login([FromBody] LoginCommand request)
        {
            return Ok(await _mediator.Send(request));
        }

        [HttpPost]
        [Route("/api/[controller]/register")]
        public async Task<IActionResult> Register([FromBody] RegisterCommand request)
        {
            return Ok(await _mediator.Send(request));
        }

        [HttpPost]
        [Route("/api/[controller]/logout")]
        public async Task<IActionResult> Logout([FromBody] LogoutCommand request)
        {
            return Ok(await _mediator.Send(request));
        }

        [HttpPut]
        [Route("/api/[controller]/refresh-token")]
        public async Task<IActionResult> RefreshToken([FromBody] RefreshTokenCommand request)
        {
            return Ok(await _mediator.Send(request));
        }
        [HttpGet]
        [Route("/api/[controller]/getbyrefreshtoken")]
        public async Task<IActionResult> GetUserByRefreshToken([FromQuery] GetUserByRefreshTokenQuery request)
        {
            return Ok(await _mediator.Send(request));
        }
    }
}
