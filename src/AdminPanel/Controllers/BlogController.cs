﻿using Application.BlogDetails.BlogCommands;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AdminPanel.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "MODERATOR,S_ADMIN,ADMIN")]
    public class BlogController : ControllerBase
    {
        private readonly IMediator _mediator;

        public BlogController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<IActionResult> PostBlog([FromForm] CreateBlogCommand command)
        {
            await _mediator.Send(command);
            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> UpdateBlog([FromForm] UpdateBlogCommand command)
        {
            await _mediator.Send(command);
            return Ok();
        }
        
        [HttpDelete]
        public async Task<IActionResult> DeleteBlog([FromBody] DeleteBlogCommand command)
        {
            await _mediator.Send(command);
            return Ok();
        }

        [HttpDelete]
        [Route("/api/[controller]/multidelete")]
        public async Task<IActionResult> MultiDeleteBlogs([FromBody] MultiDeleteBlogsCommand command)
        {
            await _mediator.Send(command);
            return Ok();
        }
    }
}
