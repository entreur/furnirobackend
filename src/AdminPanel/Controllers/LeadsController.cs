﻿using Application.AboutDetails.Commands;
using Application.AboutDetails.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AdminPanel.Controllers
{
    //Don't be confused, leads is what the contact inquiry is called on business side of things.
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "S_ADMIN,ADMIN")]
    public class LeadsController : ControllerBase
    {
        private readonly IMediator _mediator;
        public LeadsController(IMediator mediator)
        {
            _mediator = mediator;
        }
        [HttpGet]
        //Automatically sorted by created date, and paginated.
        public async Task<IActionResult> GetAllLeads([FromQuery] GetLeadsQuery request)
        {
            return Ok(await _mediator.Send(request));
        }
        [HttpPatch] 
        [Route("/api/[controller]/DeleteLead")]
        //Because we are soft deleting the items, this is a put request.
        //We are not actually deleting the item, just setting the IsDeleted to true. Still in database.
        public async Task<IActionResult> DeleteLead([FromBody] DeleteLeadCommand request)
        {
            return Ok(await _mediator.Send(request));
        }
        //getbyid
        [HttpGet]
        [Route("/api/[controller]/getleadbyid")]
        public async Task<IActionResult> GetLeadById([FromQuery] GetLeadByIdQuery request)
        {
            return Ok(await _mediator.Send(request));
        }   
    }
}
