﻿using Domain.Entities;
using Domain.Exceptions;
using Domain.Repositories;
using Infrastructure.Data;

namespace Application.Implementation
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly ApplicationDBContext _context;
        public Repository(ApplicationDBContext context)
        {
            _context = context;
        }
       
        public async Task AddAsync(T entity)
        {
            await _context.Set<T>().AddAsync(entity);   
        }

        public async Task AddAndSaveAsync(T entity)
        {
            await _context.Set<T>().AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
        //TODO: 
        //1) Move the id from string to Guid
        //2) Remove the findasync as it breaks single responsiblity.
        public async Task DeleteAsync(T entity)
        {
            //T? data = await _context.Set<T>().FindAsync(Guid.Parse(id)) 
            //    ?? throw new EntityNotFoundException<T>(id);
            _context.Set<T>().Remove(entity);
            await _context.SaveChangesAsync();
        }
        public async Task<T> UpdateAsync(T entity)
        {
            _context.Set<T>().Update(entity);
            await _context.SaveChangesAsync();
            return entity;
        }
    }
}