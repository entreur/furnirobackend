﻿using Application.ImageDetails.Commands;
using Application.Implementation;
using Application.Repositories;
using AutoMapper;
using Domain.Constants;
using Domain.Entities;
using Domain.Repositories;
using Infrastructure.Data;
using Microsoft.AspNetCore.Http;

namespace Application.ImageDetails
{
    /// <summary>
    /// Use case for Generic Image Upload Mechanism.
    /// Responsible for manipulating images both on database and server level.
    /// Requires Image<T> to be added inside AppDBContext.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ImageRepository<T> : IImageRepository<T> where T : BaseEntity
    {
        private readonly Repository<Images<T>> _repo;
        private readonly IFileManagerRepository _manager;
        private readonly IMapper _mapper;

        public ImageRepository(ApplicationDBContext context, IFileManagerRepository manager, IMapper mapper)
        {
            _repo = new(context);
            _manager = manager;
            _mapper = mapper;
        }
        #region Add
        public async Task AddImage(IEnumerable<IFormFile> images, Guid id)
        {
            string path = $"{FileUploadConstants.ROOT}/{typeof(T).Name}";
            IEnumerable<string>? data = await _manager.MultiUpload(images, path);
            foreach (string item in data)
            {
                CreateImageCommand imageCommand = new()
                {
                    ImageUrl = item,
                    ItemId = id,
                };
                Images<T>? image = _mapper.Map<Images<T>>(imageCommand);
                await _repo.AddAndSaveAsync(image);
            }
        }

        public async Task AddImage(IFormFile image, Guid id)
        {
            string path = $"{FileUploadConstants.ROOT}/{typeof(T).Name}";
            string? data = await _manager.Upload(image, path);
            CreateImageCommand imageCommand = new()
            {
                ImageUrl = data,
                ItemId = id,
            };
            Images<T>? imageEntity = _mapper.Map<Images<T>>(imageCommand);
            await _repo.AddAndSaveAsync(imageEntity);
        }

        #endregion
        public async Task UpdateImage(Images<T> entity)
        {
            await _repo.UpdateAsync(entity);
        }

        #region Delete
        public async Task DeleteImage(IEnumerable<Images<T>> images)
        {
            await DeleteImageFromDatabase(images);
            DeleteImageFromLocalStorage(images);
        }

        public async Task DeleteImage(Images<T> image)
        {
            await DeleteImageFromDatabase(image);
            DeleteImageFromLocalStorage(image);
        }

        #region DeleteImageFromDatabase
        private async Task DeleteImageFromDatabase(IEnumerable<Images<T>> images)
        {
            foreach (Images<T>? image in images)
            {
                await _repo.DeleteAsync(image);
            }
        }
        private async Task DeleteImageFromDatabase(Images<T> image)
        {
            await _repo.DeleteAsync(image);
        }
        #endregion
        #region DeleteImageFromLocalStorage
        private void DeleteImageFromLocalStorage(IEnumerable<Images<T>> images)
        {
            foreach (Images<T>? image in images)
            {
                _manager.DeleteFile(image.ImageUrl);
            }
        }
        private void DeleteImageFromLocalStorage(Images<T> image)
        {
            _manager.DeleteFile(image.ImageUrl);
        }
        #endregion
        #endregion
    }
}