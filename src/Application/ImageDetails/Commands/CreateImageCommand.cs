﻿namespace Application.ImageDetails.Commands
{
    public class CreateImageCommand
    {
        public string ImageUrl { get; set; }
        public Guid ItemId { get; set; }
    }
}
