﻿using Microsoft.AspNetCore.Http;

namespace Application.ImageDetails.Commands
{
    public class UpdateImageCommand
    {
        public Guid ItemId { get; set; }
        public IFormFile ImageFile { get; set; }
    }
}
