﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Application.ImageDetails.DTOs
{
    public class UpdateImageDTO
    {
        public Guid? ImageId { get; set; }
        [FromForm]
        public IFormFile? ImageFile { get; set; }
    }
}