﻿using Application.ImageDetails.Responses;
using Application.Response.UserResponse;

namespace Application.BlogDetails.BlogResponse
{
    public class GetBlogByIdResponse
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public UserResponseForBlog Author { get; set; }
        public BlogCategoryResponse Category { get; set; }
        public IEnumerable<ImageResponse> BlogImages { get; set; }
    }
}
