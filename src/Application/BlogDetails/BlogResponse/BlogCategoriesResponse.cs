﻿namespace Application.BlogDetails.BlogResponse
{
    /// <summary>
    /// This one is used in showing the amount of entries,
    /// inside the categories.
    /// I.E Crafts 2
    /// </summary>
    public class BlogCategoriesResponse
    {
        public string Title { get; set; }
        public int Entries { get; set; }
    }
}
