﻿using Application.ImageDetails.Responses;

namespace Application.BlogDetails.BlogResponse
{
    public class GetRecentBlogPostsResponse
    {
        public string Title { get; set; }
        public DateTime Created { get; set; }
        public IEnumerable<ImageResponse> BlogImages { get; set; }
    }
}
