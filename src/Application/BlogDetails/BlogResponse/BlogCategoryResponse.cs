﻿namespace Application.BlogDetails.BlogResponse
{
    public class BlogCategoryResponse
    {
        public string Title { get; set; }
    }
}
