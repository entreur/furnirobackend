﻿using MediatR;

namespace Application.BlogDetails.BlogCommands
{
    public class MultiDeleteBlogsCommand : IRequest
    {
        public MultiDeleteBlogsCommand()
        {
            
        }

        public MultiDeleteBlogsCommand(List<Guid> ids)
        {
            Ids = ids;
        }

        public List<Guid> Ids { get; set; }
    }
}
