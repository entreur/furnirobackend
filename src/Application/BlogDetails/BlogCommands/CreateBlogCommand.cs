﻿using MediatR;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Application.BlogDetails.BlogCommands
{
    public class CreateBlogCommand : IRequest
    {
        public CreateBlogCommand()
        {

        }

        public CreateBlogCommand(string title, string body, IEnumerable<IFormFile> images, Guid authorId, Guid categoryId)
        {
            Title = title;
            Body = body;
            AuthorId = authorId;
            CategoryId = categoryId;
            BlogImageFiles = images;
        }

        [Required]
        public string Title { get; set; }
        [Required]
        public string Body { get; set; }
        [Required]
        //Due to lack of authentication or authorization systems, we must add the user by id.
        //Once they are in place, we can set it up automatically to be associated with current user session. (Via Session Tokens)
        public Guid AuthorId { get; set; }
        [Required]
        public Guid CategoryId { get; set; }
        public IEnumerable<IFormFile> BlogImageFiles { get; set; }
    }
}
