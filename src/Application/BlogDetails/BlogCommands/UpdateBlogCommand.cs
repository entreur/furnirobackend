﻿using Application.ImageDetails.DTOs;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Application.BlogDetails.BlogCommands
{
    public class UpdateBlogCommand : IRequest
    {
        public UpdateBlogCommand()
        {

        }

        public UpdateBlogCommand(Guid id, string title, string body, Guid categoryId)
        {
            Id = id;
            Title = title;
            Body = body;
            CategoryId = categoryId;
        }

        [Required]
        public Guid Id { get; set; }
        [Required]
        public Guid CategoryId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Body { get; set; }
        //Used for Add or Update.
        public List<UpdateImageDTO>? ImageDTOs { get; set; }
        //Used For Delete
        public List<Guid>? ImageId { get; set; }
    }
}
