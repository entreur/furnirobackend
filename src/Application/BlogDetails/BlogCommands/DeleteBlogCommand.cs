﻿using MediatR;

namespace Application.BlogDetails.BlogCommands
{
    public class DeleteBlogCommand : IRequest
    {
        public DeleteBlogCommand()
        {
            
        }
        public DeleteBlogCommand(Guid id)
        {
            Id = id;
        }
        public Guid Id { get; set; }
    }
}
