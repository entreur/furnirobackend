﻿using Application.BlogDetails.BlogResponse;
using MediatR;

namespace Application.BlogDetails.BlogQueries
{
    public class GetBlogByIdQuery : IRequest<GetBlogByIdResponse>
    {
        public GetBlogByIdQuery(Guid id)
        {
            Id = id;
        }
        public Guid Id { get; set; }
    }
}
