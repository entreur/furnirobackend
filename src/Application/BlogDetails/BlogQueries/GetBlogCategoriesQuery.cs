﻿using Application.BlogDetails.BlogResponse;
using MediatR;

namespace Application.BlogDetails.BlogQueries
{
    public class GetBlogCategoriesQuery : IRequest<IEnumerable<BlogCategoriesResponse>>
    {

    }
}
