﻿using Application.BlogDetails.BlogResponse;
using MediatR;

namespace Application.BlogDetails.BlogQueries
{
    public class GetRecentBlogPostsQuery : IRequest<IEnumerable<GetRecentBlogPostsResponse>>
    {

    }
}