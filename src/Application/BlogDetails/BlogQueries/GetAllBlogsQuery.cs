﻿using Application.BlogDetails.BlogResponse;
using Application.Response.Pagination;
using MediatR;

namespace Application.BlogDetails.BlogQueries
{
    public class GetAllBlogsQuery : IRequest<Pagination<GetAllBlogsResponse>>
    {
        public GetAllBlogsQuery(string keyword, short page,short pageSize)
        {
            Keyword = keyword;
            Page = page;
            PageSize = pageSize;
        }
        public string Keyword { get; set; } = string.Empty;
        public short Page { get; set; }
        public short PageSize { get; set; }
    }
}
