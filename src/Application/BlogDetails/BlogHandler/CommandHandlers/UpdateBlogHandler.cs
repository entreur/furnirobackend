﻿using Application.BlogDetails.BlogCommands;
using Application.ImageDetails;
using Application.ImageDetails.DTOs;
using Application.Repositories;
using AutoMapper;
using Domain.Constants;
using Domain.Entities;
using Domain.Exceptions;
using Domain.Repositories;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.BlogDetails.BlogHandler.CommandHandlers
{
    public class UpdateBlogHandler : IRequestHandler<UpdateBlogCommand>
    {
        private readonly IBlogRepository _repo;
        private readonly IMapper _mapper;
        private readonly IFileManagerRepository _file;
        private readonly ImageRepository<Blog> _imageRepo;
        private readonly ApplicationDBContext _context;

        public UpdateBlogHandler(ApplicationDBContext dbContext, IFileManagerRepository file, IBlogRepository repo, IMapper mapper)
        {
            _repo = repo;
            _file = file;
            _context = dbContext;
            _mapper = mapper;
            _imageRepo = new(_context, file, mapper);
        }
        //May God have mercy upon my wretched soul for making this abomination.
        public async Task Handle(UpdateBlogCommand request, CancellationToken cancellationToken)
        {
            Blog? currentBlogData = _context.Blogs
                .Include(b => b.BlogImages)
                .AsNoTracking()
                .FirstOrDefault(b => b.Id.Equals(request.Id))
                ?? throw new EntityNotFoundException<Blog>(request.Id);

            Blog? data = _mapper.Map<Blog>(request);

            data.Created = currentBlogData.Created;
            data.AuthorId = currentBlogData.AuthorId;

            if (request.ImageDTOs != null)
            {
                foreach (UpdateImageDTO image in request.ImageDTOs)
                {
                    if (image.ImageId != null)
                    {
                        //1) Update Mode.
                        Images<Blog>? images = await _context
                            .BlogImages
                            .FirstOrDefaultAsync(b => b.Id.Equals(image.ImageId), 
                            cancellationToken: cancellationToken) 
                            ?? throw new EntityNotFoundException<Images<Blog>>(image.ImageId);
                        
                        _file.DeleteFile(images.ImageUrl);
                        images.ImageUrl = await _file.Upload(image.ImageFile, $"{FileUploadConstants.ROOT}/{nameof(Blog)}");
                    }
                    else
                    {
                        //2) Create Mode.
                        await _imageRepo
                            .AddImage(image.ImageFile
                            ?? throw new EntityNotFoundException<Images<Blog>>(), data.Id);
                    }
                }
            }
            if(request.ImageId != null)
            {
                foreach (Guid imageId in request.ImageId)
                {
                    //3) Delete Mode.
                    Images<Blog>? images = await _context
                        .BlogImages
                        .FirstOrDefaultAsync(b => b.Id.Equals(imageId), 
                        cancellationToken: cancellationToken);
                    await _imageRepo.DeleteImage(images);
                }
            }
            await _repo.UpdateAsync(data);
        }
    }
}