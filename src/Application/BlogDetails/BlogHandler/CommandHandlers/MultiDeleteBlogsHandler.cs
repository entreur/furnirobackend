﻿using Application.BlogDetails.BlogCommands;
using Domain.Repositories;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.BlogDetails.BlogHandler.CommandHandlers
{
    public class MultiDeleteBlogsHandler : IRequestHandler<MultiDeleteBlogsCommand>
    {
        private readonly IBlogRepository _repo;
        private readonly ApplicationDBContext _context;

        public MultiDeleteBlogsHandler(IBlogRepository repo, ApplicationDBContext context)
        {
            _repo = repo;
            _context = context;
        }

        public async Task Handle(MultiDeleteBlogsCommand request, CancellationToken cancellationToken)
        {
            var blogsToRemove = await _context.Blogs.Where(blog => request.Ids.Contains(blog.Id)).ToListAsync(cancellationToken: cancellationToken);
            _context.Blogs.RemoveRange(blogsToRemove);
            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
