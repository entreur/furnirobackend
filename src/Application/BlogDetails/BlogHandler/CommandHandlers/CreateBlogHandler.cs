﻿using Application.BlogDetails.BlogCommands;
using Application.ImageDetails;
using Application.Repositories;
using AutoMapper;
using Domain.Entities;
using Domain.Repositories;
using Infrastructure.Data;
using MediatR;

namespace Application.BlogDetails.BlogHandler.CommandHandlers
{
    public class CreateBlogHandler : IRequestHandler<CreateBlogCommand>
    {
        private readonly IBlogRepository _repo;
        private readonly ImageRepository<Blog> _imageRepo;
        private readonly IMapper _mapper;

        public CreateBlogHandler(IBlogRepository repo, IFileManagerRepository _file, ApplicationDBContext context, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
            _imageRepo = new(context, _file, mapper);
        }
        public async Task Handle(CreateBlogCommand request, CancellationToken cancellationToken)
        {
            Blog? data = _mapper.Map<Blog>(request);
            await _repo.AddAsync(data);
            await _imageRepo.AddImage(request.BlogImageFiles, data.Id);
            await _repo.SaveAsync();
        }
    }
}