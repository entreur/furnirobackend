﻿using Application.BlogDetails.BlogCommands;
using Application.ImageDetails;
using Application.Repositories;
using AutoMapper;
using Domain.Entities;
using Domain.Exceptions;
using Domain.Repositories;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.BlogDetails.BlogHandler.CommandHandlers
{
    public class DeleteBlogHandler : IRequestHandler<DeleteBlogCommand>
    {
        private readonly IBlogRepository _repo;
        private readonly ApplicationDBContext _context;
        private readonly ImageRepository<Blog> _imageRepository;

        public DeleteBlogHandler(IBlogRepository repo, ApplicationDBContext context, IFileManagerRepository manager, IMapper mapper)
        {
            _repo = repo;
            _context = context;
            _imageRepository = new(context,manager,mapper);
        }
        public async Task Handle(DeleteBlogCommand request, CancellationToken cancellationToken)
        {
            //Find Blog
            Blog? data = await _context.Blogs
                .FirstOrDefaultAsync(b => b.Id == request.Id, 
                cancellationToken: cancellationToken) 
                ?? throw new EntityNotFoundException<Blog>(request.Id);
            //Delete Images
            var images = await _context
                .BlogImages
                .Where(b => b.ItemId.Equals(request.Id))
                .ToListAsync(cancellationToken: cancellationToken);
            await _imageRepository.DeleteImage(images);
            //Delete blog.
            await _repo.DeleteAsync(data);
        }
    }
}
