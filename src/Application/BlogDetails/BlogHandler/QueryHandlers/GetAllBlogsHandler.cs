﻿using Application.BlogDetails.BlogQueries;
using Application.BlogDetails.BlogResponse;
using Application.Response.Pagination;
using AutoMapper;
using Domain.Entities;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.BlogDetails.BlogHandler.QueryHandlers
{
    public class GetAllBlogsHandler : IRequestHandler<GetAllBlogsQuery, Pagination<GetAllBlogsResponse>>
    {
        private readonly IMapper _mapper;
        private readonly ApplicationDBContext _context;
        public GetAllBlogsHandler(IMapper mapper, ApplicationDBContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        public async Task<Pagination<GetAllBlogsResponse>> Handle(GetAllBlogsQuery request, CancellationToken cancellationToken)
        {
            List<Blog>? blogs = await _context.Blogs
                .Include(x => x.Category)
                .Include(x => x.BlogImages)
                .Include(x => x.Author)
                .Where(x => request.Keyword == null
                || x.Title.ToLower().Contains(request.Keyword.ToLower())
                || x.Body.ToLower().Contains(request.Keyword.ToLower()))
                .ToListAsync(cancellationToken: cancellationToken);

            IEnumerable<GetAllBlogsResponse>? mapped = _mapper.Map<IEnumerable<GetAllBlogsResponse>>(blogs);
            return new Pagination<GetAllBlogsResponse>(mapped.ToList(), request.Page, request.PageSize);
        }
    }
}