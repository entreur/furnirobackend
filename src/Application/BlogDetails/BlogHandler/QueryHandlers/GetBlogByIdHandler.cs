﻿using Application.BlogDetails.BlogQueries;
using Application.BlogDetails.BlogResponse;
using AutoMapper;
using Domain.Entities;
using Domain.Repositories;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Application.BlogDetails.BlogHandler.QueryHandlers
{
    public class GetBlogByIdHandler : IRequestHandler<GetBlogByIdQuery, GetBlogByIdResponse>
    {
        private readonly IMapper _mapper;
        private readonly ApplicationDBContext _context;

        public GetBlogByIdHandler(IMapper mapper, ApplicationDBContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        public async Task<GetBlogByIdResponse> Handle(GetBlogByIdQuery request, CancellationToken cancellationToken)
        {
            Blog? blog = await _context.Blogs
                .Include(x => x.Category)
                .Include(x => x.BlogImages)
                .Include(x => x.Author)
                .FirstOrDefaultAsync(x => x.Id == request.Id,
                cancellationToken: cancellationToken);

            return _mapper.Map<GetBlogByIdResponse>(blog);
        }
    }
}
