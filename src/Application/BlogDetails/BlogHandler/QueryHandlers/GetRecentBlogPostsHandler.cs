﻿using Application.BlogDetails.BlogQueries;
using Application.BlogDetails.BlogResponse;
using AutoMapper;
using Domain.Entities;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.BlogDetails.BlogHandler.QueryHandlers
{
    public class GetRecentBlogPostsHandler : IRequestHandler<GetRecentBlogPostsQuery, IEnumerable<GetRecentBlogPostsResponse>>
    {
        private readonly IMapper _mapper;
        private readonly ApplicationDBContext _context;

        public GetRecentBlogPostsHandler(ApplicationDBContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }

        public async Task<IEnumerable<GetRecentBlogPostsResponse>> Handle(GetRecentBlogPostsQuery request, CancellationToken cancellationToken)
        {
            List<Blog>? blogs = await _context.Blogs
                .Include(x => x.Category)
                .Include(x => x.BlogImages)
                .Include(x => x.Author)
            .ToListAsync(cancellationToken: cancellationToken);
            return _mapper.Map<IEnumerable<GetRecentBlogPostsResponse>>(blogs.Take(6));
        }
    }
}
