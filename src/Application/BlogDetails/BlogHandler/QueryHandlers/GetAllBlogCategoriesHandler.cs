﻿using Application.BlogDetails.BlogQueries;
using Application.BlogDetails.BlogResponse;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.BlogDetails.BlogHandler.QueryHandlers
{
    public class GetAllBlogCategoriesHandler : IRequestHandler<GetBlogCategoriesQuery, IEnumerable<BlogCategoriesResponse>>
    {
        private readonly ApplicationDBContext _context;
        public GetAllBlogCategoriesHandler(ApplicationDBContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<BlogCategoriesResponse>> Handle(GetBlogCategoriesQuery request, CancellationToken cancellationToken)
        {
            List<BlogCategoriesResponse>? data = await _context.Blogs
                .GroupBy(blog => blog.CategoryId)
                .Select(group => new BlogCategoriesResponse
                {
                    Title = group.First().Category.Title,
                    Entries = group.Count()
                })
                .ToListAsync(cancellationToken: cancellationToken);
            return data.AsEnumerable();
        }
    }
}
