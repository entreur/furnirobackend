﻿using Application.BlogDetails.BlogCommands;
using Application.BlogDetails.BlogResponse;
using Application.ImageDetails.Commands;
using Application.ImageDetails.Responses;
using Application.Response.UserResponse;
using AutoMapper;
using Domain.Entities;
using Domain.Entities.Products;
using Domain.Entities.UserRelated;


namespace Application.BlogDetails
{
    public class BlogProfiles : Profile
    {
        public BlogProfiles()
        {
            #region Helper Mappers
            CreateMap<Images<Blog>, ImageResponse>();
            CreateMap<User, UserResponseForBlog>();
            CreateMap<Category, BlogCategoryResponse>();
            CreateMap<CreateImageCommand, Images<Blog>>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => Guid.NewGuid()))
                .ForMember(dest => dest.Created, opt => opt.MapFrom(src => DateTime.UtcNow.AddHours(4)))
                .ForMember(dest => dest.Updated, opt => opt.MapFrom(src => DateTime.UtcNow.AddHours(4)));
            #endregion
            CreateMap<Blog, GetBlogByIdResponse>();
            CreateMap<Blog, GetRecentBlogPostsResponse>();
            CreateMap<Blog, GetAllBlogsResponse>();
            CreateMap<CreateBlogCommand, Blog>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => Guid.NewGuid()))
                .ForMember(dest => dest.Created, opt => opt.MapFrom(src => DateTime.UtcNow.AddHours(4)))
                .ForMember(dest => dest.Updated, opt => opt.MapFrom(src => DateTime.UtcNow.AddHours(4)));
            CreateMap<UpdateBlogCommand, Blog>()
                .ForMember(dest => dest.Updated, opt => opt.MapFrom(src => DateTime.UtcNow.AddHours(4)));
        }
    }
}