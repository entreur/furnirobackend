﻿using Application.Implementation;
using Domain.Entities;
using Domain.Repositories;
using Infrastructure.Data;

namespace Application.BlogDetails
{
    public class BlogRepository : IBlogRepository
    {
        private readonly Repository<Blog> _repo;

        public BlogRepository(ApplicationDBContext context)
        {
            _repo = new(context);
        }


        public async Task AddAsync(Blog entity)
        {
            await _repo.AddAsync(entity);
        }

        public async Task<Blog> UpdateAsync(Blog entity)
        {
            return await _repo.UpdateAsync(entity);
        }

        public async Task SaveAsync()
        {
            await _repo.SaveAsync();
        }

        public Task AddAndSaveAsync(Blog entity)
        {
            return _repo.AddAndSaveAsync(entity);
        }

        public async Task DeleteAsync(Blog entity)
        {
            await _repo.DeleteAsync(entity);
        }
    }
}