﻿using Application.CountryAndStateDetails.Responses;
using AutoMapper;
using Domain.Entities;

namespace Application.CountryAndStateDetails
{
    public class CountryAndStateProfiles : Profile
    {
        public CountryAndStateProfiles()
        {
            CreateMap<Country, GetAllCountriesAndStatesResponse>()
                .ForMember(dest => dest.Country, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.States, opt => opt.MapFrom(src => src.States));

            CreateMap<Country, GetCountryForOrder>();

            CreateMap<State, StateResponse>();
        }
    }
}
