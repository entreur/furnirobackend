﻿using Domain.Entities;

namespace Application.CountryAndStateDetails.Responses
{
    public class GetCountryForOrder
    {
        public string Name { get; set; }
    }
}