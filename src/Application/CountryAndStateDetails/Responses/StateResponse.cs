﻿namespace Application.CountryAndStateDetails.Responses
{
    public class StateResponse
    {
        public string Title { get; set; }
    }
}