﻿namespace Application.CountryAndStateDetails.Responses
{
    public class GetAllCountriesAndStatesResponse
    {
        public string Country { get; set; }
        public List<StateResponse> States { get; set; }
    }
}
