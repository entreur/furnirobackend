﻿using Application.CountryAndStateDetails.Responses;
using MediatR;

namespace Application.CountryAndStateDetails.Queries
{
    public class GetAllCountriesAndStatesQuery : IRequest<List<GetAllCountriesAndStatesResponse>>
    {

    }
}
