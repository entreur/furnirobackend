﻿using Application.BlogDetails.BlogResponse;
using Application.CountryAndStateDetails.Queries;
using Application.CountryAndStateDetails.Responses;
using AutoMapper;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;


namespace Application.CountryAndStateDetails.Handlers
{
    public class GetAllCountriesAndStatesHandler : IRequestHandler<GetAllCountriesAndStatesQuery, List<GetAllCountriesAndStatesResponse>>
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;
        public GetAllCountriesAndStatesHandler(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<GetAllCountriesAndStatesResponse>> Handle(GetAllCountriesAndStatesQuery request, CancellationToken cancellationToken)
        {
            var data = await _context.Countries
                .Include(c => c.States)
                .ToListAsync(cancellationToken: cancellationToken);

            var response = _mapper.Map<List<GetAllCountriesAndStatesResponse>>(data);

            return response;
        }
    }
}