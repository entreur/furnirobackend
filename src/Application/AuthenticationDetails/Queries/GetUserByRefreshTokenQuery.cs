﻿using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Application.AuthenticationDetails.Queries
{
    public class GetUserByRefreshTokenQuery : IRequest<string>
    {
        [Required]
        public string Token { get; set; }
    }
}
