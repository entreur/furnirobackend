﻿using MediatR;

namespace Application.AuthenticationDetails.Commands
{
    public class RefreshTokenCommand : IRequest<string>
    {
        public string RefreshToken { get; set; } = string.Empty;
    }
}