﻿using Application.AuthenticationDetails.Response;
using MediatR;

namespace Application.AuthenticationDetails.Commands
{
    public class LoginCommand : IRequest<LoginResponse>
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; } = false;
    }
}
