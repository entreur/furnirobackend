﻿using MediatR;

namespace Application.AuthenticationDetails.Commands
{
    public class LogoutCommand : IRequest<bool>
    {
        public LogoutCommand(string refreshToken)
        {
            RefreshToken = refreshToken;
        }
        public string RefreshToken { get; set; }
    }
}
