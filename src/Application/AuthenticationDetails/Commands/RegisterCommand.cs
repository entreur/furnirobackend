﻿using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Application.AuthenticationDetails.Commands
{
    public class RegisterCommand : IRequest<bool>
    {
        [Required]
        public string Fullname { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [Required]
        public bool SubscribeToNewsletter { get; set; } = true;
    }
}
