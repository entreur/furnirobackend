﻿using Application.Implementation;
using Domain.Entities.UserRelated;
using Domain.Repositories;
using Infrastructure.Data;

namespace Application.AuthenticationDetails
{
    public class AuthenticationRepository : IAuthenticationRepository
    {
        private readonly Repository<User> _repo;

        public AuthenticationRepository(ApplicationDBContext context)
        {
            _repo = new(context);  
        }

        public async Task AddAndSaveAsync(User entity)
        {
            await _repo.AddAndSaveAsync(entity);
        }

        public async Task AddAsync(User entity)
        {
            await _repo.AddAsync(entity);
        }

        public async Task DeleteAsync(User entity)
        {
            await _repo.DeleteAsync(entity);
        }

        public async Task SaveAsync()
        {
            await _repo.SaveAsync();
        }

        async Task<User> IRepository<User>.UpdateAsync(User entity)
        {
            return await _repo.UpdateAsync(entity);
        }
    }
}
