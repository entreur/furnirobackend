﻿using Application.AuthenticationDetails.Commands;
using Application.AuthenticationDetails.Response;
using AutoMapper;
using CryptoHelper;
using Domain.Entities.UserRelated;

namespace Application.AuthenticationDetails
{
    public class UserProfiles : Profile
    {
        public UserProfiles()
        {
            #region Register
            CreateMap<RegisterCommand, User>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => Guid.NewGuid()))
                .ForMember(dest => dest.Password, opt => opt.MapFrom(src => Crypto.HashPassword(src.Password)))
                .ForMember(dest => dest.Updated, opt => opt.MapFrom(src => DateTime.UtcNow.AddHours(4)))
                .ForMember(dest => dest.Created, opt => opt.MapFrom(src => DateTime.UtcNow.AddHours(4)));
            #endregion
            //#region Login
            //CreateMap<LoginResponse, User>();
            //#endregion
        }
    }
}
