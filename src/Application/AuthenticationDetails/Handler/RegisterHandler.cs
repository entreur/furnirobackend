﻿using Application.AuthenticationDetails.Commands;
using Application.Implementation;
using AutoMapper;
using Domain.Entities;
using Domain.Entities.UserRelated;
using Domain.Exceptions;
using Domain.Repositories;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.AuthenticationDetails.Handler
{
    public class RegisterHandler : IRequestHandler<RegisterCommand, bool>
    {
        //Some friendly notes here,
        //hashing password takes place in profile (mapper) not here.
        private readonly ApplicationDBContext _context;
        private readonly IAuthenticationRepository _repo;
        private readonly IMapper _mapper;
       
        public RegisterHandler(ApplicationDBContext context,IAuthenticationRepository repo, IMapper mapper)
        {
            _repo = repo;
            _context = context;
            _mapper = mapper;
        }

        public async Task<bool> Handle(RegisterCommand request, CancellationToken cancellationToken)
        {
            if (await _context.Users.AnyAsync(x => x.Email == request.Email, cancellationToken))
            {
                throw new UserAlreadyRegisteredException(request.Email);
            }
            if (request.SubscribeToNewsletter)
            {
                Newsletter news = _mapper.Map<Newsletter>(request);
                await _context.AddAsync(news, cancellationToken);
                await _context.SaveChangesAsync(cancellationToken);
            }
            User? data = _mapper.Map<RegisterCommand, User>(request);

            await _repo.AddAndSaveAsync(data);
            return true;
        }
    }
}
