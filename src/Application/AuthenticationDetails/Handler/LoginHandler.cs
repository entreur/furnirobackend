﻿using Application.AuthenticationDetails.Commands;
using Application.AuthenticationDetails.Response;
using CryptoHelper;
using Domain.Entities.UserRelated;
using Domain.Exceptions;
using Domain.Exceptions.AuthExceptions;
using Domain.Repositories;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.AuthenticationDetails.Handler
{
    public class LoginHandler : IRequestHandler<LoginCommand, LoginResponse>
    {
        private readonly ApplicationDBContext _context;
        private readonly ITokenRepository _token;

        public LoginHandler(ApplicationDBContext context,ITokenRepository token)
        {
            _context = context;
            _token = token;
        }

        public async Task<LoginResponse> Handle(LoginCommand request, CancellationToken cancellationToken)
        {
            User? user = _context
                .Users
                .Include(x => x.UserRole)
                .FirstOrDefault(x => x.Email == request.Email)
                ?? throw new EntityNotFoundException<User>(request.Email);

            if(!Crypto.VerifyHashedPassword(user.Password,request.Password))
            {
                throw new InvalidPasswordException<User>();
            }

            return new LoginResponse()
            {
                JWT = await _token.GenerateJWTToken(user),
                RefreshToken = request.RememberMe ? await ApplyRefreshToken(user) : string.Empty,
                RefreshTokenExpirationDate = request.RememberMe ? user.RefreshTokenExpirationDate : null,
            };
        }

        private async Task<string> ApplyRefreshToken(User user)
        {
            await _token.GenerateRefreshToken(user);
            await _context.SaveChangesAsync();
            return user.RefreshToken ?? 
                throw new Exception("Critical exception happened when generating " +
                "and (or) send the Refresh Token");
        }
    }
}