﻿using Application.AuthenticationDetails.Queries;
using Domain.Entities.UserRelated;
using Domain.Exceptions.AuthExceptions;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.AuthenticationDetails.Handler
{
    public class UserByRefreshTokenHandler : IRequestHandler<GetUserByRefreshTokenQuery, string>
    {
        private readonly ApplicationDBContext _context;

        public UserByRefreshTokenHandler(ApplicationDBContext context)
        {
            _context = context;
        }

        public async Task<string> Handle(GetUserByRefreshTokenQuery request, CancellationToken cancellationToken)
        {
            User? data = await _context
                .Users
                .FirstOrDefaultAsync(u => u.RefreshToken == request.Token, 
                cancellationToken: cancellationToken) ?? throw new InvalidTokenException(request.Token);

            return data.Id.ToString();
        }
    }
}
