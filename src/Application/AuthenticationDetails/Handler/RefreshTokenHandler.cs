﻿using Application.AuthenticationDetails.Commands;
using Domain.Entities.UserRelated;
using Domain.Exceptions.AuthExceptions;
using Domain.Repositories;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.AuthenticationDetails.Handler
{
    public class RefreshTokenHandler : IRequestHandler<RefreshTokenCommand, string>
    {
        private readonly ApplicationDBContext _context;
        private readonly ITokenRepository _token;
        public RefreshTokenHandler(ApplicationDBContext context, ITokenRepository token)
        {
            _context = context;
            _token = token;
        }

        public async Task<string> Handle(RefreshTokenCommand request, CancellationToken cancellationToken)
        {
            User? user = await _context.Users
                .FirstOrDefaultAsync(x => x.RefreshToken == request.RefreshToken, 
                cancellationToken: cancellationToken)
                ?? throw new InvalidTokenException(request.RefreshToken);

            if (user.RefreshTokenExpirationDate < DateTime.UtcNow)
            {
                user.RefreshToken = string.Empty;
                user.RefreshTokenExpirationDate = null;
                await _context.SaveChangesAsync(cancellationToken);
                throw new UserTokenHasExpiredException<User>();
            }

            string token = await _token.GenerateRefreshToken(user);
            await _context.SaveChangesAsync(cancellationToken); 
            return token;
        }
    }
}