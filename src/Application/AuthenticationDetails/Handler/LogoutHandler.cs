﻿using Application.AuthenticationDetails.Commands;
using Domain.Entities.UserRelated;
using Domain.Exceptions;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.AuthenticationDetails.Handler
{
    public class LogoutHandler : IRequestHandler<LogoutCommand, bool>
    {
        private readonly ApplicationDBContext _context;
        public LogoutHandler(ApplicationDBContext context)
        {
            _context = context;
        }
        public async Task<bool> Handle(LogoutCommand request, CancellationToken cancellationToken)
        {
            User? user = await _context.Users.FirstOrDefaultAsync(u => u.RefreshToken == request.RefreshToken, cancellationToken: cancellationToken) ?? throw new EntityNotFoundException<User>(request.RefreshToken);
            user.RefreshToken = string.Empty;
            user.RefreshTokenExpirationDate = null;
            await _context.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
}
