﻿namespace Application.AuthenticationDetails.Response
{
    public class LoginResponse
    {
        public string? RefreshToken { get; set; }
        public DateTime? RefreshTokenExpirationDate { get; set; }
        public string JWT { get; set; }
    }
}