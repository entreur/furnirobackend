﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ProductDetails.ProductResponse
{
    public class TagResponse
    {
        public string Title { get; set; }
    }
}
