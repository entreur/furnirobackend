﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ProductDetails.ProductResponse
{
    public class SizeResponse
    {
        public string Title { get; set; }
        public int Quantity { get; set; }
    }
}
