﻿using Application.Response;
using Domain.Entities.Products;

namespace Application.ProductDetails.ProductResponse
{
    public class ProductResponseForOrder
    {
        public string Name { get; set; }
        public string SKU { get; set; }
        public double Price { get; set; }
        public double? DiscountPercentage { get; set; }
        public CategoryForOrderResponse Category { get; set; } 
    }
}
