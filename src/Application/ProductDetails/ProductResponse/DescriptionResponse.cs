﻿using Application.ImageDetails.Responses;

namespace Application.ProductDetails.ProductResponse
{
    public class DescriptionResponse
    {
        public string Text { get; set; }
        public List<ImageResponse> DescriptionImages { get; set; }
    }
}
