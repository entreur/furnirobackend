﻿namespace Application.ProductDetails.ProductResponse
{
    public class ReviewResponse
    {
        public string Comment { get; set; }
    }
}
