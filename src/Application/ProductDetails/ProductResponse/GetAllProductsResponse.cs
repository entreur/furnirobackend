﻿using Application.ImageDetails.Responses;

namespace Application.ProductDetails.ProductResponse
{
    public class GetAllProductsResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public double? DiscountPrice
        {
            get
            {
                return Price - (Price * DiscountPercentage / 100);
            }
        }
        public double? DiscountPercentage { get; set; }
        public ProductCategoryResponse Category { get; set; }
        public IEnumerable<SizeResponse> Size { get; set; }
        public IEnumerable<ColorResponse> Color { get; set; }
        public IEnumerable<ImageResponse> ProductImages { get; set; }
    }
}
