﻿namespace Application.ProductDetails.ProductResponse
{
    public class ProductCategoryResponse
    {
        public string Title { get; set; }
    }
}
