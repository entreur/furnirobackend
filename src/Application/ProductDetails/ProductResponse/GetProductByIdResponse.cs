﻿using Application.ImageDetails.Responses;

namespace Application.ProductDetails.ProductResponse
{
    public class GetProductByIdResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string SKU { get; set; }
        public double Price { get; set; }
        public double? DiscountPrice
        {
            get
            {
                return Price - (Price * DiscountPercentage / 100);
            }
        }
        public double? DiscountPercentage { get; set; }
        public string Summary { get; set; }
        public string AdditionalInformation { get; set; }
        public DescriptionResponse Description { get; set; }
        public ProductCategoryResponse Category { get; set; }
        public IEnumerable<ColorResponse> Color { get; set; }
        public IEnumerable<TagResponse> Tag { get; set; }
        public IEnumerable<SizeResponse> Size { get; set; }
        public IEnumerable<ReviewResponse> Review { get; set; }
        public IEnumerable<ImageResponse> ProductImages { get; set; }
        public IEnumerable<StockResponse> Stock { get; set; }
    }
}
