﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ProductDetails.ProductResponse
{
    public class StockResponse
    {
        public string Size { get; set; }
        public string Color { get; set; }
        public int Quantity { get; set; }
    }
}
