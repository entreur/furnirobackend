﻿using Application.ImageDetails.Commands;
using Application.ImageDetails.Responses;
using Application.ProductDetails.ProductCommands;
using Application.ProductDetails.ProductRequest;
using Application.ProductDetails.ProductResponse;
using AutoMapper;
using Domain.Entities;
using Domain.Entities.Products;

namespace Application.ProductDetails
{
    public class ProductProfiles : Profile
    {
        public ProductProfiles()
        {
            CreateMap<Images<Product>, ImageResponse>();
            CreateMap<Images<DescriptionOfProduct>, ImageResponse>();

            CreateMap<CreateImageCommand, Images<Product>>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => Guid.NewGuid()))
                .ForMember(dest => dest.Created, opt => opt.MapFrom(src => DateTime.UtcNow))
                .ForMember(dest => dest.Updated, opt => opt.MapFrom(src => DateTime.UtcNow));

            CreateMap<CreateImageCommand, Images<DescriptionOfProduct>>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => Guid.NewGuid()))
                .ForMember(dest => dest.Created, opt => opt.MapFrom(src => DateTime.UtcNow))
                .ForMember(dest => dest.Updated, opt => opt.MapFrom(src => DateTime.UtcNow));

            CreateMap<Review, ReviewResponse>();
            CreateMap<Stock, StockResponse>();
            CreateMap<Stock, StockRequest>();
            CreateMap<ProductColor, ColorRequest>();
            CreateMap<ProductSize, SizeRequest>();
            CreateMap<Color, ColorRequest>();
            CreateMap<Size, SizeRequest>();
            CreateMap<Category, ProductCategoryResponse>();

            CreateMap<DescriptionOfProduct, DescriptionResponse>()
                .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Text))
                .ForMember(dest => dest.DescriptionImages, opt => opt.MapFrom(src => src.DescriptionImages.Select(image => new ImageResponse
                {
                    Id = image.Id,
                    ImageUrl = image.ImageUrl
                }).ToList())); 


            CreateMap<ProductColor, ColorResponse>()
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Color.Title))
                .ForMember(dest => dest.HexCode, opt => opt.MapFrom(src => src.Color.HexCode))
                .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.Quantity));

            CreateMap<ProductTag, TagResponse>()
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Tag.Title));

            CreateMap<ProductSize, SizeResponse>()
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Size.Title))
                .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.Quantity));

            CreateMap<Product, GetProductByIdResponse>()
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.DescriptionOfProduct))
                .ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.Category))
                .ForMember(dest => dest.Color, opt => opt.MapFrom(src => src.ProductColor))
                .ForMember(dest => dest.Tag, opt => opt.MapFrom(src => src.ProductTag))
                .ForMember(dest => dest.Size, opt => opt.MapFrom(src => src.ProductSize))
                .ForMember(dest => dest.Review, opt => opt.MapFrom(src => src.Reviews))
                .ForMember(dest => dest.Stock, opt => opt.MapFrom(src => src.Stocks.Select(stock => new StockResponse
                {
                    Size = stock.Size.Title,
                    Color = stock.Color.Title,
                    Quantity = stock.Quantity
                })));

            CreateMap<Product, GetAllProductsResponse>()
                .ForMember(dest => dest.Size, opt => opt.MapFrom(src => src.ProductSize))
                .ForMember(dest => dest.Color, opt => opt.MapFrom(src => src.ProductColor));

            CreateMap<CreateProductCommand, Product>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => Guid.NewGuid()))
                .ForMember(dest => dest.Created, opt => opt.MapFrom(src => DateTime.UtcNow))
                .ForMember(dest => dest.Updated, opt => opt.MapFrom(src => DateTime.UtcNow))
                .ForMember(dest => dest.ProductTag, opt => opt.MapFrom(src => src.TagId.Select(id => new ProductTag { TagId = id })))
                .ForMember(dest => dest.ProductColor, opt => opt.MapFrom(src => src.Colors.Select(c => new ProductColor { ColorId = c.ColorId, Quantity = c.Quantity })))
                .ForMember(dest => dest.ProductSize, opt => opt.MapFrom(src => src.Sizes.Select(s => new ProductSize { SizeId = s.SizeId, Quantity = s.Quantity })))
                .ForMember(dest => dest.Stocks, opt => opt.MapFrom(src => src.Stocks.Select(stockDto => new Stock
                {
                    SizeId = stockDto.SizeId,
                    ColorId = stockDto.ColorId,
                    Quantity = stockDto.Quantity
                })));

            CreateMap<CreateProductCommand, DescriptionOfProduct>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => Guid.NewGuid()))
                .ForMember(dest => dest.Created, opt => opt.MapFrom(src => DateTime.UtcNow))
                .ForMember(dest => dest.Updated, opt => opt.MapFrom(src => DateTime.UtcNow));

            CreateMap<UpdateProductCommand, Product>()
                .ForMember(dest => dest.Updated, opt => opt.MapFrom(src => DateTime.UtcNow))
                .ForMember(dest => dest.ProductTag, opt => opt.MapFrom(src => src.TagId.Select(id => new ProductTag { TagId = id })))
                .ForMember(dest => dest.ProductColor, opt => opt.MapFrom(src => src.Colors.Select(c => new ProductColor { ColorId = c.ColorId, Quantity = c.Quantity })))
                .ForMember(dest => dest.ProductSize, opt => opt.MapFrom(src => src.Sizes.Select(s => new ProductSize { SizeId = s.SizeId, Quantity = s.Quantity })))
                .ForMember(dest => dest.Stocks, opt => opt.MapFrom(src => src.Stocks.Select(stockDto => new Stock
                 {
                     SizeId = stockDto.SizeId,
                     ColorId = stockDto.ColorId,
                     Quantity = stockDto.Quantity
                 })));

            CreateMap<CartItem, CartItemResponse>()
                .ForMember(dest => dest.ProductId,opt => opt.MapFrom(src => src.ProductId));

            CreateMap<WishList, WishListItemsResponse>()
                .ForMember(dest => dest.ProductId, opt => opt.MapFrom(src => src.ProductId));

            CreateMap<ShoppingCart, ShoppingCartResponse>()
            .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
            .ForMember(dest => dest.CartItems, opt => opt.MapFrom(src => src.Items.Select(item => new CartItemResponse
            {})));
        }
    }
}