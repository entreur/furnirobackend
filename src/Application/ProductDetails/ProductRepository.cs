﻿using Application.ImageDetails;
using Application.Implementation;
using Application.Repositories;
using AutoMapper;
using Domain.Constants;
using Domain.Entities;
using Domain.Entities.Products;
using Domain.Repositories;
using Infrastructure.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace Application.ProductDetails
{
    public class ProductRepository : IProductRepository
    {
        private readonly Repository<Product> _repo;
        private readonly ApplicationDBContext _context;
        private readonly IFileManagerRepository _file;
        private readonly ImageRepository<Product> _imageRepository;

        public ProductRepository(ApplicationDBContext context, IFileManagerRepository file, IMapper mapper)
        {
            _context = context;
            _file = file;
            _repo = new(_context);
            _imageRepository = new(_context, _file, mapper);
        }

        public async Task AddAsync(Product entity)
        {
            await _repo.AddAsync(entity);
        }

        public async Task<Product> UpdateAsync(Product entity)
        {
            return await _repo.UpdateAsync(entity);
        }

        public async Task UploadProductImage(IEnumerable<IFormFile> images, Guid id)
        {
            await _imageRepository.AddImage(images, id);
        }

        public async Task SaveAsync()
        {
            await _repo.SaveAsync();
        }

        public async Task AddAndSaveAsync(Product entity)
        {
            await _repo.AddAndSaveAsync(entity);
        }

        public Task<Product> GetProductById(Guid productId)
        {
            throw new NotImplementedException();
        }

        public async Task DeleteAsync(Product entity)
        {
            await _repo.DeleteAsync(entity);
        }
    }
}