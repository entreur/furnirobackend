﻿using Application.ProductDetails.ProductResponse;
using Domain.Entities.Products;
using MediatR;

namespace Application.ProductDetails.ProductQueries
{
    public class GetProductByIdQuery : IRequest<GetProductByIdResponse>
    {
        public GetProductByIdQuery(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}
