﻿using Application.ProductDetails.ProductResponse;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ProductDetails.ProductQueries
{
    public class GetWishListQuery : IRequest<ShoppingCartResponse>
    {
        public GetWishListQuery()
        {

        }
        public Guid UserId { get; set; }
    }
}


