﻿using Application.ProductDetails.ProductResponse;
using Application.Response.Pagination;
using MediatR;

namespace Application.ProductDetails.ProductQueries
{
    public class GetAllProductsQuery : IRequest<Pagination<GetAllProductsResponse>>
    {
        public GetAllProductsQuery(string keyword, short page, double? minPrice, double? maxPrice, string? size, string? color)
        {
            Keyword = keyword;
            Page = page;
            MinPrice = minPrice;
            MaxPrice = maxPrice;
            Size = size;
            Color = color;
        }

        public string Keyword { get; set; } = string.Empty;
        public short Page { get; set; }
        public double? MinPrice { get; set; }
        public double? MaxPrice { get; set; }
        public string? Size { get; set; } = string.Empty;
        public string? Color { get; set; } = string.Empty;
    }
}