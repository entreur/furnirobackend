﻿namespace Application.ProductDetails.ProductRequest
{
    public class ColorRequest
    {
        public Guid ColorId { get; set; }
        public int Quantity { get; set; }
    }
}
