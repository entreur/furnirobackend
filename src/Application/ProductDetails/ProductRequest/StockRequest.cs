﻿namespace Application.ProductDetails.ProductRequest
{
    public class StockRequest
    {
        public Guid SizeId { get; set; }
        public Guid ColorId { get; set; }
        public int Quantity { get; set; }
    }
}
