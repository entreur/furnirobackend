﻿namespace Application.ProductDetails.ProductRequest
{
    public class SizeRequest
    {
        public Guid SizeId { get; set; }
        public int Quantity { get; set; }
    }
}
