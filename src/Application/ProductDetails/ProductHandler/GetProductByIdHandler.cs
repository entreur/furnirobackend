﻿using Application.ProductDetails.ProductQueries;
using Application.ProductDetails.ProductResponse;
using AutoMapper;
using Domain.Entities.Products;
using Domain.Exceptions;
using Domain.Repositories;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.ProductDetails.ProductHandler
{
    public class GetProductByIdHandler : IRequestHandler<GetProductByIdQuery, GetProductByIdResponse>
    {
        private readonly IProductRepository _repo;
        private readonly IMapper _mapper;
        private readonly ApplicationDBContext _context;
        public GetProductByIdHandler(IProductRepository repo, IMapper mapper, ApplicationDBContext context)
        {
            _repo = repo;
            _mapper = mapper;
            _context = context;
        }

        public async Task<GetProductByIdResponse> Handle(GetProductByIdQuery request, CancellationToken cancellationToken)
        {
            var product = await _context.Products
                .Include(p => p.Category)
                .Include(p => p.ProductImages)
                .Include(p => p.ProductColor)
                .ThenInclude(p => p.Color)
                .Include(p => p.ProductTag)
                .ThenInclude(p => p.Tag)
                .Include(p => p.ProductSize)
                .ThenInclude(p => p.Size)
                .Include(p => p.DescriptionOfProduct)
                .ThenInclude(p => p.DescriptionImages)
                .Include(p => p.Reviews)
                .Include(p => p.Stocks)
                .ToListAsync(cancellationToken: cancellationToken);

            Product? result = product
                .FirstOrDefault(p => p.Id.Equals(request.Id)) 
                ?? throw new EntityNotFoundException<Product>(request.Id);
            return _mapper.Map<GetProductByIdResponse>(result);
        }
    }
}
