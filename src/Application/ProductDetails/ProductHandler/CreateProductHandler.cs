﻿using Application.ProductDetails.ProductCommands;
using AutoMapper;
using Domain.Entities;
using Domain.Entities.Products;
using Domain.Repositories;
using Infrastructure.Data;
using MediatR;

public class CreateProductHandler : IRequestHandler<CreateProductCommand, Unit>
{
    private readonly IProductRepository _repo;
    private readonly IMapper _mapper;
    private readonly ApplicationDBContext _context;
    private readonly IImageRepository<Product> _imageRepository;
    private readonly IImageRepository<DescriptionOfProduct> _descriptionOfProducRepository;


    public CreateProductHandler(IProductRepository repo, IMapper mapper, ApplicationDBContext context, IImageRepository<Product> imageRepository, IImageRepository<DescriptionOfProduct> descriptionOfProducRepository)
    {
        _repo = repo;
        _mapper = mapper;
        _context = context;
        _imageRepository = imageRepository;
        _descriptionOfProducRepository = descriptionOfProducRepository;

    }

    public async Task<Unit> Handle(CreateProductCommand request, CancellationToken cancellationToken)
    {
        var product = _mapper.Map<Product>(request);
        var description = _mapper.Map<DescriptionOfProduct>(request);
        product.DescriptionOfProductId = description.Id;

        var productColors = request.Colors.Select(colorDto => new ProductColor
        {
            ColorId = colorDto.ColorId,
            Quantity = colorDto.Quantity
        }).ToList();
        product.ProductColor = productColors;

        var productSizes = request.Sizes.Select(sizeDto => new ProductSize
        {
            SizeId = sizeDto.SizeId,
            Quantity = sizeDto.Quantity
        }).ToList();
        product.ProductSize = productSizes;

        var productStocks = request.Stocks.Select(stockDto => new Stock
        {
            SizeId = stockDto.SizeId,
            ColorId = stockDto.ColorId,
            Quantity = stockDto.Quantity
        }).ToList();
        product.Stocks = productStocks;

        await _repo.AddAsync(product);
        await _context.Descriptions.AddAsync(description);
        await _repo.SaveAsync();

        await _imageRepository.AddImage(request.ProductImageFiles, product.Id);
        await _descriptionOfProducRepository.AddImage(request.DescriptionOfProductImageFiles, description.Id);

        return Unit.Value;
    }
}
