﻿using Application.CartDetails.Queries;
using Application.ProductDetails.ProductQueries;
using Application.ProductDetails.ProductResponse;
using AutoMapper;
using Domain.Entities.Products;
using Domain.Exceptions;
using Domain.Repositories;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.ProductDetails.ProductHandler
{
    public class GetWishListHandler : IRequestHandler<GetWishListQuery, ShoppingCartResponse>
    {
        private readonly IShoppingCartRepository _shoppingCartRepository;
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public GetWishListHandler(IShoppingCartRepository shoppingCartRepository, ApplicationDBContext context, IMapper mapper)
        {
            _shoppingCartRepository = shoppingCartRepository;
            _context = context;
            _mapper = mapper;
        }

        public async Task<ShoppingCartResponse> Handle(GetWishListQuery request, CancellationToken cancellationToken)
        {
            var shoppingCart = await _context.ShoppingCart
                .Include(c => c.WishListItems)
                .ThenInclude(i => i.Product)
                .FirstOrDefaultAsync(c => c.UserId == request.UserId)
                ?? throw new EntityNotFoundException<ShoppingCart>(request.UserId);

            var wishListItemsResponse = _mapper.Map<List<WishListItemsResponse>>(shoppingCart.WishListItems);

            var totalDiscountedPrice = Math.Round(shoppingCart.WishListItems.Sum(item => CalculateItemPrice(item)), 2);

            var shoppingCartResponse = new ShoppingCartResponse()
            {
                UserId = shoppingCart.UserId,
                WishListItems = wishListItemsResponse,
                TotalPrice = totalDiscountedPrice
            };

            return shoppingCartResponse;
        }

        private double CalculateItemPrice(WishList item)
        {
            double itemPrice;

            if (item.Product.DiscountPercentage.HasValue)
            {
                double discountFactor = 1 - (item.Product.DiscountPercentage.Value / 100);
                itemPrice = item.Product.Price * discountFactor;
            }
            else
            {
                itemPrice = item.Product.Price;
            }

            return itemPrice * item.Quantity;
        }
    }
}
