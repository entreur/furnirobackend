﻿using Application.ProductDetails.ProductCommands;
using Domain.Entities.Products;
using Domain.Exceptions;
using Domain.Repositories;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ProductDetails.ProductHandler
{
    public class DeleteCartItemsHandler : IRequestHandler<DeleteCartItemsCommand>
    {
        private readonly ApplicationDBContext _context;

        public DeleteCartItemsHandler(ApplicationDBContext context)
        {
            _context = context;
        }

        public async Task Handle(DeleteCartItemsCommand request, CancellationToken cancellationToken)
        {
            CartItem? data = await _context.CartItems
               .FirstOrDefaultAsync(b => b.Id == request.Id,
               cancellationToken: cancellationToken)
               ?? throw new EntityNotFoundException<CartItem>(request.Id);

            _context.CartItems.Remove(data);
            await _context.SaveChangesAsync();
        }
    }
}
