﻿using Application.ProductDetails.ProductCommands;
using Domain.Exceptions;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WishList = Domain.Entities.Products.WishList;

namespace Application.ProductDetails.ProductHandler
{
    public class DeleteWishListHandler : IRequestHandler<DeleteWishListCommand>
    {
        private readonly ApplicationDBContext _context;

        public DeleteWishListHandler(ApplicationDBContext context)
        {
            _context = context;
        }

        public async Task Handle(DeleteWishListCommand request, CancellationToken cancellationToken)
        {
            WishList? data = await _context.WishList
               .FirstOrDefaultAsync(b => b.Id == request.Id,
               cancellationToken: cancellationToken)
               ?? throw new EntityNotFoundException<WishList>(request.Id);

            _context.WishList.Remove(data);
            await _context.SaveChangesAsync();
        }
    }
}
