﻿using Application.ProductDetails.ProductQueries;
using Application.ProductDetails.ProductResponse;
using Application.Response.Pagination;
using AutoMapper;
using Domain.Repositories;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.ProductDetails.ProductHandler
{
    public class GetAllProductsHandler : IRequestHandler<GetAllProductsQuery, Pagination<GetAllProductsResponse>>
    {
        private readonly IMapper _mapper;
        private readonly ApplicationDBContext _context;
        private readonly short _pageSize = 12;

        public GetAllProductsHandler(IMapper mapper, ApplicationDBContext applicationDBContext)
        {
            _mapper = mapper;
            _context = applicationDBContext;
        }

        public async Task<Pagination<GetAllProductsResponse>> Handle(GetAllProductsQuery request, CancellationToken cancellationToken)
        {
            var products = await _context.Products
                 .Include(p => p.Category)
                 .Include(p => p.ProductSize)
                 .ThenInclude(ps => ps.Size)
                 .Include(p => p.ProductColor)
                 .ThenInclude(pc => pc.Color)
                 .Include(p => p.ProductImages.OrderBy(pi => pi.Created).Take(1))
                 .ToListAsync();

            if (!string.IsNullOrEmpty(request.Keyword))
            {
                products = products.Where(p => p.Name.Contains(request.Keyword, StringComparison.OrdinalIgnoreCase)).ToList();
            }

            if (request.MinPrice != 0)
            {
                products = products.Where(p => p.Price >= request.MinPrice.Value).ToList();
            }

            if (request.MaxPrice != 0)
            {
                products = products.Where(p => p.Price <= request.MaxPrice.Value).ToList();
            }

            if (!string.IsNullOrEmpty(request.Size))
            {
                products = products
                    .Where(p => p.ProductSize.Any(ps => ps.Size != null && ps.Size.Title.Equals(request.Size, StringComparison.OrdinalIgnoreCase)))
                    .ToList();
            }

            if (!string.IsNullOrEmpty(request.Color))
            {
                products = products
                    .Where(p => p.ProductColor.Any(pc => pc.Color != null && pc.Color.Title.Equals(request.Color, StringComparison.OrdinalIgnoreCase)))
                    .ToList();
            }

            var mapped = _mapper.Map<IEnumerable<GetAllProductsResponse>>(products).ToList();
            return new Pagination<GetAllProductsResponse>(mapped, request.Page, _pageSize);
        }
    }
}