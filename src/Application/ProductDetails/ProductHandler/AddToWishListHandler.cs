﻿using Application.ProductDetails.ProductCommands;
using Domain.Entities.Products;
using Domain.Exceptions;
using Domain.Repositories;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;


namespace Application.ProductDetails.ProductHandler
{
    public class AddToWishListHandler : IRequestHandler<AddToWishListCommand, bool>
    {
        private readonly IShoppingCartRepository _shoppingCartRepository;
        private readonly ApplicationDBContext _context;
        private readonly IProductRepository _productRepository;

        public AddToWishListHandler(IShoppingCartRepository shoppingCartRepository, IProductRepository productRepository, ApplicationDBContext context)
        {
            _shoppingCartRepository = shoppingCartRepository;
            _productRepository = productRepository;
            _context = context;
        }

        public async Task<bool> Handle(AddToWishListCommand request, CancellationToken cancellationToken)
        {
            var product = await _context.Products
                .Include(p => p.ProductSize)
                .Include(p => p.ProductColor)
                .FirstOrDefaultAsync(p => p.Id == request.ProductId);

            if (product != null)
            {
                var userCart = await _context.ShoppingCart
                    .Include(cart => cart.WishListItems)
                    .FirstOrDefaultAsync(x => x.UserId == request.UserId, cancellationToken: cancellationToken);

                if (userCart == null)
                {
                    userCart = new ShoppingCart { UserId = request.UserId, WishListItems = new List<WishList>() };
                }

                userCart.WishListItems ??= new List<WishList>();

                var existingItem = userCart.WishListItems.FirstOrDefault(item =>
                    item.Product.Id == request.ProductId &&
                    item.SizeId == request.SizeId &&
                    item.ColorId == request.ColorId);

                if (existingItem != null)
                {
                    existingItem.Quantity += request.Quantity;
                }

                else
                {
                    var newItem = new WishList
                    {
                        Product = product,
                        Quantity = request.Quantity,
                        SizeId = request.SizeId,
                        ColorId = request.ColorId
                    };

                    userCart.WishListItems.Add(newItem);
                }

                await _shoppingCartRepository.SaveAsync();

                return true;
            }

            return false;
        }
    }
}