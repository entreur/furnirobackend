﻿using Application.ImageDetails;
using Application.ImageDetails.DTOs;
using Application.ProductDetails.ProductCommands;
using Application.Repositories;
using AutoMapper;
using Domain.Constants;
using Domain.Entities;
using Domain.Entities.Products;
using Domain.Exceptions;
using Domain.Repositories;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.ProductDetails.ProductHandler
{
    public class UpdateProductHandler : IRequestHandler<UpdateProductCommand>
    {
        private readonly IProductRepository _repo;
        private readonly IMapper _mapper;
        private readonly ApplicationDBContext _context;
        private readonly IFileManagerRepository _file;
        private readonly ImageRepository<Product> _imageRepository;

        public UpdateProductHandler(ApplicationDBContext dbContext, IFileManagerRepository file, IProductRepository repo, IMapper mapper)
        {
            _repo = repo;
            _file = file;
            _context = dbContext;
            _mapper = mapper;
            _imageRepository = new(_context, file, mapper);
        }


        public async Task Handle(UpdateProductCommand request, CancellationToken cancellationToken)
        {
            Product? current = await _context.Products
                .Include(p => p.Category)
                .Include(p => p.ProductImages)
                .Include(p => p.ProductColor)
                .ThenInclude(p => p.Color)
                .Include(p => p.ProductTag)
                .ThenInclude(p => p.Tag)
                .Include(p => p.ProductSize)
                .ThenInclude(p => p.Size)
                .Include(p => p.DescriptionOfProduct)
                .Include(p => p.Reviews)
                .Include(p => p.Stocks)
                .AsNoTracking()
                .FirstOrDefaultAsync(p => p.Id == request.Id);

            Product data = _mapper.Map<Product>(request);
            data.Created = current.Created;
            data.Updated = DateTime.UtcNow;

            if (request.ImageDTOs != null)
            {
                foreach (UpdateImageDTO image in request.ImageDTOs)
                {
                    if (image.ImageId != null)
                    {
                        Images<Product>? images = await _context
                            .ProductImages
                            .FirstOrDefaultAsync(b => b.Id.Equals(image.ImageId),
                            cancellationToken: cancellationToken)
                            ?? throw new EntityNotFoundException<Images<Product>>(image.ImageId);

                        _file.DeleteFile(images.ImageUrl);
                        images.ImageUrl = await _file.Upload(image.ImageFile, $"{FileUploadConstants.ROOT}/{nameof(Product)}");
                    }
                    else
                    {
                       await _imageRepository.AddImage(image.ImageFile ?? throw new EntityNotFoundException<Images<Product>>(), data.Id);
                    }
                }
            }
            if (request.ImageId != null)
            {
                foreach (Guid imageId in request.ImageId)
                {
                    //3) Delete Mode.
                    Images<Product>? images = await _context
                        .ProductImages
                        .FirstOrDefaultAsync(b => b.Id.Equals(imageId),
                        cancellationToken: cancellationToken);
                    await _imageRepository.DeleteImage(images);
                }
            }
            await _repo.UpdateAsync(data);

        }
    }
}