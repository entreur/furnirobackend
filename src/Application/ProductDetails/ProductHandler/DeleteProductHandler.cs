﻿using Application.ImageDetails;
using Application.ProductDetails.ProductCommands;
using Application.Repositories;
using AutoMapper;
using Domain.Entities;
using Domain.Entities.Products;
using Domain.Exceptions;
using Domain.Repositories;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.ProductDetails.ProductHandler
{
    public class DeleteProductHandler : IRequestHandler<DeleteProductCommand>
    {
        private readonly IProductRepository _repo;
        private readonly ApplicationDBContext _context;
        private readonly ImageRepository<Product> _imageRepository;

        public DeleteProductHandler(IProductRepository repo, ApplicationDBContext context, IFileManagerRepository manager, IMapper mapper)
        {
            _repo = repo;
            _context = context;
            _imageRepository = new(context, manager, mapper);
        }

        public async Task Handle(DeleteProductCommand request, CancellationToken cancellationToken)
        {
            Product? data = await _context.Products
                .FirstOrDefaultAsync(b => b.Id == request.Id,
                cancellationToken: cancellationToken)
                ?? throw new EntityNotFoundException<Product>(request.Id);

            //Delete Images
            var images = await _context
                .ProductImages
                .Where(b => b.ItemId.Equals(request.Id))
                .ToListAsync(cancellationToken: cancellationToken);

            await _imageRepository.DeleteImage(images);

            await _repo.DeleteAsync(data);
        }
    }
}
