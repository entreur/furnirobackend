﻿using Application.ImageDetails.DTOs;
using Application.ProductDetails.ProductRequest;
using Domain.Entities;
using Domain.Entities.Products;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Application.ProductDetails.ProductCommands
{
    public class UpdateProductCommand : IRequest
    {
        public UpdateProductCommand()
        {

        }
        public UpdateProductCommand(Guid id, string name, string sku, double price, double discountPercentage, Guid categoryId, string summary, string additionalInformation, IEnumerable<ColorRequest> colors, IEnumerable<Guid> tagId, IEnumerable<SizeRequest> sizeId, Guid descriptionOfProductId, IEnumerable<StockRequest> stocks, List<IFormFile> updatedImages)
        {
            Id = id;
            Name = name;
            SKU = sku;
            Price = price;
            DiscountPercentage = discountPercentage;
            CategoryId = categoryId;
            Summary = summary;
            AdditionalInformation = additionalInformation;
            TagId = tagId;
            DescriptionOfProductId = descriptionOfProductId;
            Colors = colors;
            Sizes = sizeId;
            Stocks = stocks;
        }

        [Required]
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [MaxLength(8, ErrorMessage = "SKU's cannot exceed 8.")]
        public string SKU { get; set; }
        [Required]
        public double Price { get; set; }
        public double? DiscountPercentage { get; set; }
        [Required]
        public string Summary { get; set; }
        [Required]
        public string AdditionalInformation { get; set; }
        [Required]
        public Guid CategoryId { get; set; }
        [Required]
        public Guid DescriptionOfProductId { get; set; }
        [Required]
        public IEnumerable<Guid> TagId { get; set; }
        [Required]
        public IEnumerable<ColorRequest> Colors { get; set; }
        [Required]
        public IEnumerable<SizeRequest> Sizes { get; set; }
        [Required]
        public IEnumerable<StockRequest> Stocks { get; set; }
        public List<UpdateImageDTO>? ImageDTOs { get; set; }
        public List<Guid>? ImageId { get; set; }

    }
}
