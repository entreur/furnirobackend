﻿using Application.ProductDetails.ProductRequest;
using Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Application.ProductDetails.ProductCommands
{
    public class CreateProductCommand : IRequest<Unit>
    {
        public CreateProductCommand()
        {
            
        }
        public CreateProductCommand(string name, string sku, double price, double discountPercentage, Guid categoryId, string summary, string additionalInformation, IEnumerable<ColorRequest> colors, IEnumerable<Guid> tagId, IEnumerable<SizeRequest> sizes, Guid descriptionOfProductId, IEnumerable<IFormFile> images, IEnumerable<IFormFile> descriptionImages, IEnumerable<StockRequest> stocks, string text)
        {
            Name = name;
            SKU = sku;
            Price = price;
            DiscountPercentage = discountPercentage;
            CategoryId = categoryId;
            Summary = summary;
            AdditionalInformation = additionalInformation;
            TagId = tagId;
            Text = text;
            ProductImageFiles = images;
            DescriptionOfProductImageFiles = descriptionImages;
            Colors = colors;
            Sizes = sizes;
            Stocks = stocks;
        }

        [Required]
        public string Name { get; set; }
        [Required]
        [MaxLength(8, ErrorMessage = "SKU's cannot exceed 8.")]
        public string SKU { get; set; }
        [Required]
        public double Price { get; set; }
        public double? DiscountPercentage { get; set; }
        [Required]
        public string Summary { get; set; }
        [Required]
        public string AdditionalInformation { get; set; }
        [Required]
        public Guid CategoryId { get; set; }
        [Required]
        public string Text { get; set; }
        public IEnumerable<Guid> TagId { get; set; }
        [Required]
        public IEnumerable<ColorRequest> Colors { get; set; }
        [Required]
        public IEnumerable<SizeRequest> Sizes { get; set; }
        [Required]
        public IEnumerable<StockRequest> Stocks { get; set; }
        public IEnumerable<IFormFile> ProductImageFiles { get; set; }
        public IEnumerable<IFormFile> DescriptionOfProductImageFiles { get; set; }

    }
}
