﻿using MediatR;

namespace Application.ProductDetails.ProductCommands
{
    public class DeleteProductCommand : IRequest
    {
        public DeleteProductCommand()
        {

        }
        public DeleteProductCommand(Guid id)
        {
            Id = id;
        }
        public Guid Id { get; set; }
    }
}
