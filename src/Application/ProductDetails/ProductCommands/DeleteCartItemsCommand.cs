﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ProductDetails.ProductCommands
{
    public class DeleteCartItemsCommand : IRequest
    {
        public DeleteCartItemsCommand()
        {

        }
        public DeleteCartItemsCommand(Guid id)
        {
            Id = id;
        }
        public Guid Id { get; set; }
    }
}
