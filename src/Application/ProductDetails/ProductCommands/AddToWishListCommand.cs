﻿using MediatR;

namespace Application.ProductDetails.ProductCommands
{
    public class AddToWishListCommand : IRequest<bool>
    {
        public Guid UserId { get; set; }
        public Guid ProductId { get; set; }
        public Guid SizeId { get; set; }
        public Guid ColorId { get; set; }
        public short Quantity { get; set; }
    }
}
