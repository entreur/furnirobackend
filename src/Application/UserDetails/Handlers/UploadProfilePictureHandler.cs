﻿using Application.Repositories;
using Application.UserDetails.Commands;
using Domain.Constants;
using Domain.Entities.UserRelated;
using Domain.Exceptions;
using Infrastructure.Data;
using MediatR;

namespace Application.UserDetails.Handlers
{
    public class UploadProfilePictureHandler : IRequestHandler<ProfilePictureCommand, bool>
    {
        private readonly ApplicationDBContext _context;
        private readonly IFileManagerRepository _fileManager;
        public UploadProfilePictureHandler(ApplicationDBContext context,IFileManagerRepository fileManager)
        {
            _context = context;
            _fileManager = fileManager;
        }

        public async Task<bool> Handle(ProfilePictureCommand request, CancellationToken cancellationToken)
        {
           User? user = _context.Users
                .FirstOrDefault(x => x.Id == request.UserId) 
                ?? throw new EntityNotFoundException<User>(request.UserId);
           user.ProfileImageUrl = await _fileManager.Upload(request.ImageFile,$"{FileUploadConstants.ROOT}/{typeof(User).Name}");
           await _context.SaveChangesAsync(cancellationToken);
           return true;
        }
    }
}
