﻿using Application.UserDetails.Commands;
using CryptoHelper;
using Domain.Entities.UserRelated;
using Domain.Exceptions.AuthExceptions;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.UserDetails.Handlers
{
    public class ChangePasswordHandler : IRequestHandler<ChangePasswordCommand, bool>
    {
        private readonly ApplicationDBContext _context;
        public ChangePasswordHandler(ApplicationDBContext context)
        {
            _context = context;
        }

        public async Task<bool> Handle(ChangePasswordCommand request, CancellationToken cancellationToken)
        {
            if (request.OldPassword.Equals(request.NewPassword))
            {
                throw new NewOldPasswordCantBeSameException<User>();
            }

            User? user = await _context.Users
                .FirstOrDefaultAsync(u => u.Email == request.Email, cancellationToken) 
                ?? throw new InvalidTokenException(request.Email);


            if (!Crypto.VerifyHashedPassword(user.Password,request.OldPassword))
            {
                throw new InvalidPasswordException<User>();
            }

            user.Password = Crypto.HashPassword(request.NewPassword);
            await _context.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
}