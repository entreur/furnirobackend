﻿using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Application.UserDetails.Commands
{
    public class ChangePasswordCommand : IRequest<bool>
    {
        #region Constructor
        public ChangePasswordCommand(string email, string newPassword, string confirmPassword, string oldpassword)
        {
            Email = email;
            NewPassword = newPassword;
            OldPassword = oldpassword;
            ConfirmNewPassword = confirmPassword;
        }
        public ChangePasswordCommand()
        {
            
        }
        #endregion
        [Required]
        public string OldPassword { get; set; }
        [Required]
        public string NewPassword { get; set; }
        [Required]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmNewPassword { get; set; }
        [Required]
        public string Email { get; set; }

    }
}