﻿using MediatR;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Application.UserDetails.Commands
{
    public class ProfilePictureCommand : IRequest<bool>
    {
        public ProfilePictureCommand()
        {
            
        }

        public ProfilePictureCommand(IFormFile imageFile)
        {
            ImageFile = imageFile;
        }

        [Required]
        public Guid UserId { get; set; }
        [Required]
        public IFormFile ImageFile { get; set; }
    }
}