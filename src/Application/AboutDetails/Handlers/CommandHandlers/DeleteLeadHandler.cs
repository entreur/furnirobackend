﻿using Application.AboutDetails.Commands;
using Domain.Entities;
using Domain.Exceptions;
using Infrastructure.Data;
using MediatR;

namespace Application.AboutDetails.Handlers.CommandHandlers
{
    public class DeleteLeadHandler : IRequestHandler<DeleteLeadCommand, bool>
    {
        public DeleteLeadHandler(ApplicationDBContext context)
        {
            _context = context;
        }
        private readonly ApplicationDBContext _context;
        public async Task<bool> Handle(DeleteLeadCommand request, CancellationToken cancellationToken)
        {
            ContactInqury? data = await _context.Leads.FindAsync(request.Id)
                ?? throw new EntityNotFoundException<ContactInqury>(request.Id);
            data.IsDeleted = true;
            await _context.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
}
