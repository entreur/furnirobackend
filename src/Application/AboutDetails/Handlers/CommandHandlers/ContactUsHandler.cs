﻿using Application.AboutDetails.Commands;
using AutoMapper;
using Domain.Entities;
using Infrastructure.Data;
using MediatR;

namespace Application.AboutDetails.Handlers.CommandHandlers
{
    public class ContactUsHandler : IRequestHandler<ContactUsCommand, bool>
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public ContactUsHandler(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<bool> Handle(ContactUsCommand request, CancellationToken cancellationToken)
        {
            ContactInqury? mapped = _mapper.Map<ContactInqury>(request);
            _context.Leads.Add(mapped);
            await _context.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
}
