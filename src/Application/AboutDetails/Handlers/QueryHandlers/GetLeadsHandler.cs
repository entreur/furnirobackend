﻿using Application.AboutDetails.ContactResponse;
using Application.AboutDetails.Queries;
using Application.Response.Pagination;
using AutoMapper;
using Domain.Entities;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.AboutDetails.Handlers.QueryHandlers
{
    public class GetLeadsHandler : IRequestHandler<GetLeadsQuery, Pagination<ContactInquryResponse>> //Was List
    {
        public GetLeadsHandler(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public async Task<Pagination<ContactInquryResponse>> Handle(GetLeadsQuery request, CancellationToken cancellationToken)
        {
            List<ContactInqury>? data = await _context.Leads
                .OrderByDescending(i => i.Created)
                .ToListAsync(cancellationToken: cancellationToken);
            IEnumerable<ContactInquryResponse>? mapped = _mapper.Map<IEnumerable<ContactInquryResponse>>(data);
            return new Pagination<ContactInquryResponse>(mapped.ToList(), request.Page, request.PageSize);
        }
    }
}
