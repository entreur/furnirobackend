﻿using Application.AboutDetails.ContactResponse;
using Application.AboutDetails.Queries;
using AutoMapper;
using Domain.Entities;
using Domain.Exceptions;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.AboutDetails.Handlers.QueryHandlers
{
    public class GetContactDetailsHandler : IRequestHandler<GetContactDetailsQuery, ContactDetailsResponse>
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;
        public GetContactDetailsHandler(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ContactDetailsResponse> Handle(GetContactDetailsQuery request, CancellationToken cancellationToken)
        {
            ContactDetails? data = await _context.SiteContactDetails.FirstOrDefaultAsync(cancellationToken: cancellationToken)
                ?? throw new EntityNotFoundException<ContactDetails>();
            return _mapper.Map<ContactDetailsResponse>(data);
        }
    }
}
