﻿using Application.AboutDetails.Queries;
using Application.AboutDetails.Response;
using AutoMapper;
using Domain.Entities;
using Domain.Exceptions;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.AboutDetails.Handlers.QueryHandlers
{
    public class GetLeadByIdHandler : IRequestHandler<GetLeadByIdQuery, GetLeadByIdResponse>
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;
        public GetLeadByIdHandler(ApplicationDBContext context,IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<GetLeadByIdResponse> Handle(GetLeadByIdQuery request, CancellationToken cancellationToken)
        {
            ContactInqury? data = await _context.Leads
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken: cancellationToken)
                ?? throw new EntityNotFoundException<ContactInqury>(request.Id);
            return _mapper.Map<GetLeadByIdResponse>(data);
        }
    }
}
