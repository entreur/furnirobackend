﻿using Application.AboutDetails.ContactResponse;
using Domain.Entities;
using MediatR;

namespace Application.AboutDetails.Queries
{
    public class GetContactDetailsQuery : IRequest<ContactDetailsResponse>
    {

    }
}
