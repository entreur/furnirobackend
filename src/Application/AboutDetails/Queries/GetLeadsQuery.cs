﻿using Application.AboutDetails.ContactResponse;
using Application.Response.Pagination;
using MediatR;

namespace Application.AboutDetails.Queries
{
    public class GetLeadsQuery : IRequest<Pagination<ContactInquryResponse>>
    {
        public GetLeadsQuery()
        {
            
        }
        public GetLeadsQuery(short page, short pageSize)
        {
            Page = page;
            PageSize = pageSize;
        }
        public short Page { get; set; }
        public short PageSize { get; set; }
    }
}
