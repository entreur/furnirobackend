﻿using Application.AboutDetails.Response;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Application.AboutDetails.Queries
{
    public class GetLeadByIdQuery : IRequest<GetLeadByIdResponse>
    {
        [Required]
        public Guid Id { get; set; }
    }
}
