﻿using System.ComponentModel.DataAnnotations;

namespace Application.AboutDetails.Response
{
    public class GetLeadByIdResponse
    {
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
    }
}
