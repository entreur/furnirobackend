﻿namespace Application.AboutDetails.ContactResponse
{
    public class ContactDetailsResponse
    {
        public string PhoneNumberHotline { get; set; }
        public string PhoneNumberMobile { get; set; }
        public string WorkHoursOnWeekdays { get; set; }
        public string WorkHoursOnWeekends { get; set; }
        public string Address { get; set; }
    }
}
