﻿using System.ComponentModel.DataAnnotations;

namespace Application.AboutDetails.ContactResponse
{
    public class ContactInquryResponse
    {
        public Guid Id { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public DateTime MessageReceivedDate { get; set; }

    }
}
