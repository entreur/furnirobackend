﻿using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Application.AboutDetails.Commands
{
    public class ContactUsCommand : IRequest<bool>
    {
        [Required]
        public string Fullname { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "Not a valid email address")]
        public string Email { get; set; }
        [Required]
        [MaxLength(250,ErrorMessage = "Cannot exceed 250")]
        public string Subject { get; set; }
        [Required]
        [MaxLength(1000,ErrorMessage = "Cannot exceed 1000")]
        public string Message { get; set; }
    }
}
