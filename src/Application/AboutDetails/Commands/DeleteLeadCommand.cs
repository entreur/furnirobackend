﻿using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Application.AboutDetails.Commands
{
    public class DeleteLeadCommand : IRequest<bool>
    {
        [Required]
        public Guid Id { get; set; }
    }
}
