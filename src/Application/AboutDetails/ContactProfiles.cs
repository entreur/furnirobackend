﻿using Application.AboutDetails.Commands;
using Application.AboutDetails.ContactResponse;
using Application.AboutDetails.Response;
using AutoMapper;
using Domain.Entities;

namespace Application.AboutDetails
{
    public class ContactProfiles : Profile
    {
        public ContactProfiles()
        {
            CreateMap<ContactUsCommand, ContactInqury>()
                .ForMember(dest => dest.Created, opt => opt.MapFrom(src => DateTime.UtcNow.AddHours(4)))
                .ForMember(dest => dest.Updated, opt => opt.MapFrom(src => DateTime.UtcNow.AddHours(4)));

            CreateMap<ContactDetails, ContactDetailsResponse>();

            CreateMap<ContactInqury, ContactInquryResponse>()
                .ForMember(dest => dest.MessageReceivedDate, opt => opt.MapFrom(src => src.Created));

            CreateMap<ContactInqury, GetLeadByIdResponse>();
        }    
    }
}
