﻿using Application.NewsletterDetails.Responses;
using Application.Response.Pagination;
using MediatR;

namespace Application.NewsletterDetails.Queries
{
    public class GetAllNewsletterQuery : IRequest<Pagination<GetAllNewsletterSubscribersResponse>>
    {
        public GetAllNewsletterQuery()
        {

        }
        public GetAllNewsletterQuery(short page, short pageSize)
        {
            Page = page;
            PageSize = pageSize;
        }
        public short Page { get; set; }
        public short PageSize { get; set; }
    }
}
