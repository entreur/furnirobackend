﻿namespace Application.NewsletterDetails.Responses
{
    public class GetAllNewsletterSubscribersResponse
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public DateTime Created { get; set; }
    }
}
