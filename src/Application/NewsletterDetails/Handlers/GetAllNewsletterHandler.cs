﻿using Application.AboutDetails.ContactResponse;
using Application.NewsletterDetails.Queries;
using Application.NewsletterDetails.Responses;
using Application.Response.Pagination;
using AutoMapper;
using Domain.Entities;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.NewsletterDetails.Handlers
{
    internal class GetAllNewsletterHandler : IRequestHandler<GetAllNewsletterQuery, Pagination<GetAllNewsletterSubscribersResponse>>
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;
        public GetAllNewsletterHandler(ApplicationDBContext context,IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        async Task<Pagination<GetAllNewsletterSubscribersResponse>> IRequestHandler<GetAllNewsletterQuery, Pagination<GetAllNewsletterSubscribersResponse>>.Handle(GetAllNewsletterQuery request, CancellationToken cancellationToken)
        {
            List<Newsletter> data = await _context.NewsletterSubscriptions
                .OrderByDescending(i => i.Created)
                .ToListAsync(cancellationToken: cancellationToken);
            IEnumerable<GetAllNewsletterSubscribersResponse> mapped = _mapper.Map<IEnumerable<GetAllNewsletterSubscribersResponse>>(data);
            return new Pagination<GetAllNewsletterSubscribersResponse>(mapped.ToList(), request.Page, request.PageSize);
        }
    }
}
