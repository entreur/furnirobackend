﻿using Application.NewsletterDetails.Commands;
using Domain.Entities;
using Domain.Exceptions;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.NewsletterDetails.Handlers
{
    public class UnsubscribeHandler : IRequestHandler<UnsubscribeFromNewsletterCommand, bool>
    {

        public UnsubscribeHandler(ApplicationDBContext context)
        {
            _context = context;  
        }
        private readonly ApplicationDBContext _context;

        public async Task<bool> Handle(UnsubscribeFromNewsletterCommand request, CancellationToken cancellationToken)
        {
            Newsletter? data = await _context.NewsletterSubscriptions
                .FirstOrDefaultAsync(x => x.Email == request.Email, cancellationToken: cancellationToken)
                ?? throw new EntityNotFoundException<Newsletter>(request.Email);
            data.IsReceiver = false;
            await _context.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
}