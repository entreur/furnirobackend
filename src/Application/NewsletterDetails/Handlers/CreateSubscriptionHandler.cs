﻿using Application.NewsletterDetails.Commands;
using AutoMapper;
using Domain.Entities;
using Infrastructure.Data;
using MediatR;

namespace Application.NewsletterDetails.Handlers
{
    public class CreateSubscriptionHandler : IRequestHandler<CreateSubscriptionCommand, bool>
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;
        public CreateSubscriptionHandler(ApplicationDBContext context,IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<bool> Handle(CreateSubscriptionCommand request, CancellationToken cancellationToken)
        {
            await _context.NewsletterSubscriptions
                .AddAsync(_mapper.Map<Newsletter>(request), cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
}