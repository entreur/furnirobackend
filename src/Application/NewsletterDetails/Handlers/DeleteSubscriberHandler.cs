﻿using Application.NewsletterDetails.Commands;
using Domain.Entities;
using Domain.Exceptions;
using Infrastructure.Data;
using MediatR;

namespace Application.NewsletterDetails.Handlers
{
    public class DeleteSubscriberHandler : IRequestHandler<DeleteNewsletterCommand, bool>
    {
        private readonly ApplicationDBContext _context;
        public DeleteSubscriberHandler(ApplicationDBContext context)
        {
            _context = context;
        }
        public async Task<bool> Handle(DeleteNewsletterCommand request, CancellationToken cancellationToken)
        {
            Newsletter? data = await _context
                .NewsletterSubscriptions
                .FindAsync(request.Id) ?? throw new EntityNotFoundException<Newsletter>(request.Id);
            data.IsDeleted = true;
            await _context.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
}
