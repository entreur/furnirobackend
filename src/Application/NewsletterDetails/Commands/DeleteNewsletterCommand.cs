﻿using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Application.NewsletterDetails.Commands
{
    public class DeleteNewsletterCommand : IRequest<bool>
    {
        [Required]
        public Guid Id { get; set; }
    }
}
