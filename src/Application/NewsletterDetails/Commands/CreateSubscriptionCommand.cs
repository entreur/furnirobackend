﻿using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Application.NewsletterDetails.Commands
{
    public class CreateSubscriptionCommand : IRequest<bool>
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
