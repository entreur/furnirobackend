﻿using Application.AuthenticationDetails.Commands;
using Application.NewsletterDetails.Commands;
using Application.NewsletterDetails.Responses;
using AutoMapper;
using Domain.Entities;

namespace Application.NewsletterDetails
{
    public class NewsletterProfile : Profile
    {
        public NewsletterProfile()
        {
            CreateMap<Newsletter, GetAllNewsletterSubscribersResponse>();
            CreateMap<CreateSubscriptionCommand,Newsletter>()
                .ForMember(dest => dest.Created, opt => opt.MapFrom(src => DateTime.UtcNow.AddHours(4)))
                .ForMember(dest => dest.Updated, opt => opt.MapFrom(src => DateTime.UtcNow.AddHours(4)));

            CreateMap<RegisterCommand, Newsletter>()
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.Created, opt => opt.MapFrom(src => DateTime.UtcNow.AddHours(4)))
                .ForMember(dest => dest.Updated, opt => opt.MapFrom(src => DateTime.UtcNow.AddHours(4)));
        }
    }
}
