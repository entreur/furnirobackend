﻿namespace Application.Response
{
    public class CategoryForOrderResponse
    {
        public string Title { get; set; }
    }
}
