﻿namespace Application.Response.UserResponse
{
    public class UserResponseForOrder
    {
        public string Id { get; set; }
        public string Fullname { get; set; }
    }
}