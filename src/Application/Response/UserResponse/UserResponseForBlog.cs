﻿namespace Application.Response.UserResponse
{
    public class UserResponseForBlog
    {
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string ProfileImageUrl { get; set; }
    }
}
