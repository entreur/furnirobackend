﻿namespace Application.Response.ProductResponse
{
    public class ProductCategoryResponse
    {
        public string Title { get; set; }
    }
}
