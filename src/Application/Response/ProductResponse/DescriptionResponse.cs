﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Response.ProductResponse
{
    public class DescriptionResponse
    {
        public string Text { get; set; }
    }
}
