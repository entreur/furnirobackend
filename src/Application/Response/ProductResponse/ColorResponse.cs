﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Response.ProductResponse
{
    public class ColorResponse
    {
        public string Title { get; set; }
        public string HexCode { get; set; }
    }
}
