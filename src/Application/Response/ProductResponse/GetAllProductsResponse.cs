﻿using Application.ImageDetails.Responses;
using Application.Response.ProductResponse;

namespace Application.Response.ProductResponse
{
    public class GetAllProductsResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string SKU { get; set; }
        public double Price { get; set; }
        public string Summary { get; set; }
        public string AdditionalInformation { get; set; }
        public DescriptionResponse Description { get; set; }
        public ProductCategoryResponse Category { get; set; }
        public IEnumerable<ColorResponse> Color { get; set; }
        public IEnumerable<TagResponse> Tag { get; set; }
        public IEnumerable<SizeResponse> Size { get; set; }
        public IEnumerable<ImageResponse> ProductImages { get; set; }
    }
}
