﻿namespace Application.CartDetails.Responses
{
    public class ShoppingCartResponseForOrder
    {
        public List<CartItemResponseForOrder> Items { get; set; }
    }
}
