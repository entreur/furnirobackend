﻿using Application.ProductDetails.ProductResponse;

namespace Application.CartDetails.Responses
{
    public class CartItemResponseForOrder
    {
        public ProductResponseForOrder Product { get; set; }
        public short Quantity { get; set; }
    }
}