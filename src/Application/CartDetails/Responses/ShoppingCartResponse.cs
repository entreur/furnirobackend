﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ProductDetails.ProductResponse
{
    public class ShoppingCartResponse
    {
        public Guid UserId { get; set; }
        public List<CartItemResponse> CartItems { get; set; }
        public List<WishListItemsResponse> WishListItems { get; set; }
        public double TotalPrice { get; set; }
    }
}
