﻿using Application.ProductDetails.ProductResponse;
using Domain.Entities.Products;
using MediatR;

namespace Application.CartDetails.Queries
{
    public class GetCartQuery : IRequest<ShoppingCartResponse> //Was ShoppingCart
    {
        public GetCartQuery()
        {
            
        }
        public Guid UserId { get; set; }
    }
}
