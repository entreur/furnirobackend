﻿using Application.CartDetails.Commands;
using Application.ProductDetails.ProductCommands;
using AutoMapper;
using Domain.Entities.Products;
using Domain.Exceptions;
using Domain.Repositories;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;


namespace Application.CartDetails.Handlers
{
    public class AddToCartHandler : IRequestHandler<AddToCartCommand, bool>
    {
        private readonly IShoppingCartRepository _shoppingCartRepository;
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public AddToCartHandler(
            IShoppingCartRepository shoppingCartRepository,
            ApplicationDBContext context,
            IMapper mapper)
        {
            _shoppingCartRepository = shoppingCartRepository;
            _context = context;
            _mapper = mapper;
        }

        public async Task<bool> Handle(AddToCartCommand request, CancellationToken cancellationToken)
        {
            var product = await _context.Products
                .Include(p => p.ProductSize)
                .Include(p => p.ProductColor)
                .FirstOrDefaultAsync(p => p.Id == request.ProductId);

            if (product != null)
            {
                var userCart = await _context.ShoppingCart
                    .Include(cart => cart.Items)
                    .FirstOrDefaultAsync(x => x.UserId == request.UserId, cancellationToken: cancellationToken);

                if (userCart == null)
                {
                    userCart = new ShoppingCart { UserId = request.UserId, Items = new List<CartItem>() };
                }

                userCart.Items ??= new List<CartItem>();


                var existingItem = userCart.Items.FirstOrDefault(item =>
                    item.Product.Id == request.ProductId &&
                    item.SizeId == request.SizeId &&
                    item.ColorId == request.ColorId);

                if (existingItem != null)
                {
                    existingItem.Quantity += request.Quantity;
                }

                else
                {
                    var newItem = new CartItem
                    {
                        Product = product,
                        Quantity = request.Quantity,
                        SizeId = request.SizeId,
                        ColorId = request.ColorId
                    };

                    userCart.Items.Add(newItem);
                }

                await _shoppingCartRepository.SaveAsync();

                return true;
            }

            return false;
        }
    }
}