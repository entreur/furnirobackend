using Application.CartDetails.Queries;
using Application.ProductDetails.ProductResponse;
using AutoMapper;
using Domain.Entities.Products;
using Domain.Exceptions;
using Domain.Repositories;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.CartDetails.Handlers
{
    public class GetCartHandler : IRequestHandler<GetCartQuery, ShoppingCartResponse>
    {
        private readonly IShoppingCartRepository _shoppingCartRepository;
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public GetCartHandler(IShoppingCartRepository shoppingCartRepository, ApplicationDBContext context, IMapper mapper)
        {
            _shoppingCartRepository = shoppingCartRepository;
            _context = context;
            _mapper = mapper;
        }

        public async Task<ShoppingCartResponse> Handle(GetCartQuery request, CancellationToken cancellationToken)
        {
            var shoppingCart = await _context.ShoppingCart
                .Include(c => c.Items)
                .ThenInclude(i => i.Product)
                .FirstOrDefaultAsync(c => c.UserId == request.UserId) 
                ?? throw new EntityNotFoundException<ShoppingCart>(request.UserId);

            var cartItemsResponse = _mapper.Map<List<CartItemResponse>>(shoppingCart.Items);

            var totalDiscountedPrice = Math.Round(shoppingCart.Items.Sum(item => CalculateItemPrice(item)), 2);

            var shoppingCartResponse = new ShoppingCartResponse()
            {
                UserId = shoppingCart.UserId,
                CartItems = cartItemsResponse,
                TotalPrice = totalDiscountedPrice
            };

            return shoppingCartResponse;
        }

        private double CalculateItemPrice(CartItem item)
        {
            double itemPrice;

            if (item.Product.DiscountPercentage.HasValue)
            {
                double discountFactor = 1 - (item.Product.DiscountPercentage.Value / 100);
                itemPrice = item.Product.Price * discountFactor;
            }
            else
            {
                itemPrice = item.Product.Price;
            }

            return itemPrice * item.Quantity;
        }
    }
}
