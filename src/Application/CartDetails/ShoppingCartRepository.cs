﻿using Application.Implementation;
using Domain.Entities.Products;
using Domain.Repositories;
using Infrastructure.Data;

namespace Application.CartDetails
{
    public class ShoppingCartRepository : IShoppingCartRepository
    {
        private readonly Repository<ShoppingCart> _repo;
        private readonly ApplicationDBContext _context;
        public ShoppingCartRepository(ApplicationDBContext context)
        {
            _context = context;
            _repo = new(_context);
        }

        public async Task AddAndSaveAsync(ShoppingCart entity)
        {
            await _repo.AddAndSaveAsync(entity);
        }

        public async Task AddAsync(ShoppingCart entity)
        {
            await _repo.AddAsync(entity);
        }

        public async Task DeleteAsync(ShoppingCart entity)
        {
            await _repo.DeleteAsync(entity);
        }

        //public Task<ShoppingCart> GetByUserIdAsync(Guid userId)
        //{
        //    throw new NotImplementedException();
        //}

        public async Task SaveAsync()
        {
            await _repo.SaveAsync();
        }

        public async Task<ShoppingCart> UpdateAsync(ShoppingCart entity)
        {
            return await _repo.UpdateAsync(entity);
        }
    }
}
