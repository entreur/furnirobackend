﻿using Application.CartDetails.Commands;
using Application.CartDetails.Responses;
using AutoMapper;
using Domain.Entities.Products;

namespace Application.CartDetails
{
    public class CartProfiles : Profile
    {
        public CartProfiles()
        {
            //CreateMap<CartItem, CartItemResponse>()
            //    .ForMember(dest => dest.ProductId, opt => opt.MapFrom(src => src.ProductId));
            //CreateMap<ShoppingCart, ShoppingCartResponse>()
            //.ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
            //.ForMember(dest => dest.CartItems, opt => opt.MapFrom(src => src.Items.Select(item => new CartItemResponse
            //{
            //    // Map properties from your ShoppingCartItem entity to ShoppingCartItemResponse DTO
            //    // For example: ProductId = item.ProductId, Quantity = item.Quantity, etc.
            //})));

            CreateMap<AddToCartCommand, CartItem>()//Was ShoppingCart
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => Guid.NewGuid()))
                .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.Quantity))
                .ForMember(dest => dest.Updated, opt => opt.MapFrom(src => DateTime.UtcNow))
                .ForMember(dest => dest.Created, opt => opt.MapFrom(src => DateTime.UtcNow));
        }
    }
}