﻿using Application.Response.UserResponse;
using Application.CartDetails.Responses;
using Application.CountryAndStateDetails.Responses;

namespace Application.CheckoutDetails.Responses
{
    public class GetOrderResponse
    {
        public Guid Id { get; set; }
        public UserResponseForOrder User { get; set; }
        public string? CompanyName
        {
            get
            {
                return string.IsNullOrEmpty(_companyName) ? null : _companyName;
            }
            set
            {
                _companyName = value;
            }
        }
        public GetCountryForOrder Country { get; set; }
        public StateResponse State { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string? AdditionalInfo
        {
            get
            {
                return string.IsNullOrEmpty(_additionalInformation) ? null : _additionalInformation;
            }
            set
            {
                _additionalInformation = value;
            }
        }
        public ShoppingCartResponseForOrder Cart { get; set; }
        public Guid ShoppingCartId { get; set; }
        public bool OrderIsConfirmed { get; set; } = false;

        private string? _companyName = string.Empty;
        private string? _additionalInformation = string.Empty;
    }
}