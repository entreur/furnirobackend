﻿using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Application.CheckoutDetails.Commands
{
    public class CreateOrderCommand : IRequest<bool>
    {
        [Required]
        public string Fullname { get; set; }
        public string? CompanyName { get; set; } = string.Empty;
        [Required]
        public string City { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string ZipCode { get; set; }
        [Required]
        [Phone]
        public string PhoneNumber { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [MaxLength(1000, ErrorMessage = "Cannot exceed 1000")]
        public string? AdditionalInfo { get; set; } = string.Empty;

        [Required]
        public Guid UserId { get; set; }
        [Required]
        public Guid CountryId { get; set; }
        [Required]
        public Guid StateId { get; set; }
        [Required]
        public Guid ShoppingCartId { get; set; }
    }
}