﻿using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Application.CheckoutDetails.Commands
{
    public class ConfirmOrderCommand : IRequest<bool>
    {
        [Required]
        public Guid OrderId { get; set; }
    }
}
