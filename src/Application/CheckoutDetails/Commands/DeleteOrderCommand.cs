﻿using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Application.CheckoutDetails.Commands
{
    public class DeleteOrderCommand : IRequest<bool>
    {
        [Required]
        public Guid Id { get; set; }
    }
}
