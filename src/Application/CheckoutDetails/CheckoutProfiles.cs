﻿using Application.CartDetails.Responses;
using Application.CheckoutDetails.Commands;
using Application.CheckoutDetails.Responses;
using Application.ProductDetails.ProductResponse;
using Application.Response;
using Application.Response.UserResponse;
using AutoMapper;
using Domain.Entities;
using Domain.Entities.Products;
using Domain.Entities.UserRelated;

namespace Application.CheckoutDetails
{
    public class CheckoutProfiles : Profile
    {
        public CheckoutProfiles()
        {
            CreateMap<CreateOrderCommand, Checkout>()
                .ForMember(dest => dest.Created, opt => opt.MapFrom(src => DateTime.UtcNow))
                .ForMember(dest => dest.Updated, opt => opt.MapFrom(src => DateTime.UtcNow));

            CreateMap<Checkout, GetOrderResponse>()
                .ForMember(dest => dest.State, opt => opt.MapFrom(src => src.State));

            #region Helper Mappings
            CreateMap<User, UserResponseForOrder>();
            CreateMap<ShoppingCart, ShoppingCartResponseForOrder>();
            CreateMap<CartItem, CartItemResponseForOrder > ();
            CreateMap<Product, ProductResponseForOrder>();
            CreateMap<Category, CategoryForOrderResponse>();
            #endregion
        }
    }
}