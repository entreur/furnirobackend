﻿using Application.Implementation;
using Domain.Entities;
using Domain.Repositories;
using Infrastructure.Data;

namespace Application.CheckoutDetails
{
    public class OrderRepository : IOrderRepository
    {
        private readonly Repository<Checkout> _repo;
        public OrderRepository(ApplicationDBContext context)
        {
            _repo = new(context);
        }

        public async Task AddAndSaveAsync(Checkout entity)
        {
            await _repo.AddAndSaveAsync(entity);
        }

        public async Task AddAsync(Checkout entity)
        {
            await _repo.AddAsync(entity);
        }

        public async Task DeleteAsync(Checkout entity)
        {
            await _repo.DeleteAsync(entity);
        }

        public async Task SaveAsync()
        {
            await _repo.SaveAsync();
        }

        public async Task<Checkout> UpdateAsync(Checkout entity)
        {
            return await _repo.UpdateAsync(entity);
        }
    }
}
