﻿using Application.CheckoutDetails.Responses;
using Application.Response.Pagination;
using MediatR;

namespace Application.CheckoutDetails.Queries
{
    public class GetAllOrdersQuery : IRequest<Pagination<GetOrderResponse>>
    {
        public GetAllOrdersQuery()
        {
            
        }

        public GetAllOrdersQuery(short page, short pageSize)
        {
            Page = page;
            PageSize = pageSize;
        }

        public short Page { get; set; }
        public short PageSize { get; set; }
    }
}