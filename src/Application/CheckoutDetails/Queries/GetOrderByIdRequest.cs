﻿using Application.CheckoutDetails.Responses;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Application.CheckoutDetails.Queries
{
    public class GetOrderByIdRequest : IRequest<GetOrderResponse>
    {
        public GetOrderByIdRequest()
        {
            
        }
        public GetOrderByIdRequest(Guid id)
        {
            Id = id;
        }

        [Required]
        public Guid Id { get; set; }
    }
}
