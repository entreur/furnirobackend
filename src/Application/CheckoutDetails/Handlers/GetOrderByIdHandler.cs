﻿using Application.CheckoutDetails.Queries;
using Application.CheckoutDetails.Responses;
using AutoMapper;
using Domain.Entities;
using Domain.Exceptions;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.CheckoutDetails.Handlers
{
    public class GetOrderByIdHandler : IRequestHandler<GetOrderByIdRequest, GetOrderResponse>
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;

        public GetOrderByIdHandler(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<GetOrderResponse> Handle(GetOrderByIdRequest request, CancellationToken cancellationToken)
        {
            Checkout? data = await _context.Orders
                .Include(o => o.Country)
                .Include(c => c.State)
                .Include(o => o.User)
                .Include(o => o.Cart)
                .ThenInclude(c => c.Items)
                .ThenInclude(i => i.Product)
                .ThenInclude(p => p.Category)
                .FirstOrDefaultAsync(o => o.Id == request.Id, cancellationToken: cancellationToken) 
                ?? throw new EntityNotFoundException<Checkout>(request.Id);
            return _mapper.Map<GetOrderResponse>(data);
        }
    }
}
