﻿using Application.CheckoutDetails.Commands;
using AutoMapper;
using Domain.Entities;
using Domain.Entities.Products;
using Domain.Exceptions;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.CheckoutDetails.Handlers
{
    public class CreateOrderHandler : IRequestHandler<CreateOrderCommand, bool>
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;
        public CreateOrderHandler(ApplicationDBContext context,IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<bool> Handle(CreateOrderCommand request, CancellationToken cancellationToken)
        {
            //Step 1 : Find if a cart for this order exists.
            //If it doesn't, don't allow user to create an order.
            ShoppingCart? associatedCart = await _context
                .ShoppingCart
                .FirstOrDefaultAsync(x => x.Id == request.ShoppingCartId && x.UserId == request.UserId, cancellationToken: cancellationToken) 
                ?? throw new EntityNotFoundException<ShoppingCart>(request.ShoppingCartId);

            //Step 2 : Find the state and if it isn't valid , throw an exception.
            Country country = await _context
                .Countries.Include(c => c.States)
                .Where(c => c.Id == request.CountryId &&
                c.States.Any(s => s.Id == request.StateId))
                .FirstOrDefaultAsync(cancellationToken)
                ?? throw new EntityNotFoundException<Country>(request.CountryId);

            //Step 3 : Create an order.
            var mapped = _mapper.Map<Checkout>(request);

            //Step 4 : Add the order to the database.
            _context.Orders.Add(mapped);
            await _context.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
}