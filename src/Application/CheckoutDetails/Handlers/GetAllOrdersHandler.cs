﻿using Application.BlogDetails.BlogResponse;
using Application.CheckoutDetails.Queries;
using Application.CheckoutDetails.Responses;
using Application.Response.Pagination;
using AutoMapper;
using Domain.Entities;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.CheckoutDetails.Handlers
{
    public class GetAllOrdersHandler : IRequestHandler<GetAllOrdersQuery, Pagination<GetOrderResponse>>
    {
        private readonly ApplicationDBContext _context;
        private readonly IMapper _mapper;
        public GetAllOrdersHandler(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<Pagination<GetOrderResponse>> Handle(GetAllOrdersQuery request, CancellationToken cancellationToken)
        {
            List<Checkout>? data = await _context.Orders
                .Include(o => o.Country)
                .Include(c => c.State)
                .Include(o => o.User)
                .Include(o => o.Cart)
                .ThenInclude(c => c.Items)
                .ThenInclude(i => i.Product)
                .ThenInclude(p => p.Category)
                .ToListAsync(cancellationToken: cancellationToken);

            IEnumerable<GetOrderResponse>? mapped = _mapper.Map<List<GetOrderResponse>>(data);
            return new Pagination<GetOrderResponse>(mapped.ToList(), request.Page, request.PageSize);
        }
    }
}