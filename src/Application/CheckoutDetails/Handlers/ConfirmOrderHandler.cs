﻿using Application.CheckoutDetails.Commands;
using Domain.Entities;
using Domain.Exceptions;
using Domain.Repositories;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.CheckoutDetails.Handlers
{
    public class ConfirmOrderHandler : IRequestHandler<ConfirmOrderCommand, bool>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly ApplicationDBContext _context;
        public ConfirmOrderHandler(IOrderRepository repo,ApplicationDBContext context)
        {
            _orderRepository = repo;
            _context = context;
        }

        public async Task<bool> Handle(ConfirmOrderCommand request, CancellationToken cancellationToken)
        {
            Checkout? data = await _context.Orders
                .FirstOrDefaultAsync(o => o.Id == request.OrderId, cancellationToken: cancellationToken)
                ?? throw new EntityNotFoundException<Checkout>(request.OrderId);
            data.OrderIsConfirmed = true;
            await _orderRepository.UpdateAsync(data);
            await _orderRepository.SaveAsync();
            return true;
        }
    }
}
