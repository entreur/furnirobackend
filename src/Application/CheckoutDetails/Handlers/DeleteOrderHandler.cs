﻿using Application.CheckoutDetails.Commands;
using Domain.Entities;
using Domain.Exceptions;
using Domain.Repositories;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.CheckoutDetails.Handlers
{
    internal class DeleteOrderHandler : IRequestHandler<DeleteOrderCommand, bool>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly ApplicationDBContext _context;
        public DeleteOrderHandler(IOrderRepository repository, ApplicationDBContext context)
        {
            _orderRepository = repository;
            _context = context;
        }

        public async Task<bool> Handle(DeleteOrderCommand request, CancellationToken cancellationToken)
        {
            Checkout? data = await _context.Orders
                .FirstOrDefaultAsync(c => c.Id == request.Id, cancellationToken: cancellationToken)
                ?? throw new EntityNotFoundException<Checkout>(request.Id);
            await _orderRepository.DeleteAsync(data);
            return true;
        }
    }
}
