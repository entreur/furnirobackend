﻿using Application.Response.ProductResponse;
using Domain.Entities.Products;
using MediatR;

namespace Application.Query.ProductQueries
{
    public class GetAllProductsQuery : IRequest<IEnumerable<GetAllProductsResponse>>
    {

    }
}
