﻿using Application.Response.ProductResponse;
using Domain.Entities.Products;
using MediatR;

namespace Application.Query.ProductQueries
{
    public class GetProductByIdQuery : IRequest<GetProductByIdResponse>
    {
        public GetProductByIdQuery(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}
